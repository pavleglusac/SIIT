#include <sstream>

#include "FiniteStateMachine.h"

using namespace std;

const TokenType FiniteStateMachine::stateToTokenTable[NUM_STATE] = {
	/*state 0*/		T_NO_TYPE,
	/*state 1*/		T_NO_TYPE,
	/*state 2*/		T_ID,
	/*state 3*/		T_IF,
	/*state 4*/		T_ID,
	/*state 5*/		T_ID,
	/*state 6*/		T_ID,
	/*state 7*/		T_THEN,
	/*state 8*/		T_ID,
	/*state 9*/		T_ID,
	/*state A*/		T_ID,
	/*state B*/		T_ELSE,
	/*state C*/		T_ID,
	/*state D*/		T_MINUS,
	/*state E*/		T_PLUS,
	/*state F*/		T_EQ,
	/*state G*/		T_SEMI,
	/*state H*/		T_WHITE_SPACE,
	/*state I*/		T_ERROR,
	/*state J*/		T_NUM,
	/*state K*/		T_REAL,
	/*state M*/		T_COMMENT,
};

const char FiniteStateMachine::supportedCharacters[NUM_OF_CHARACTERS] =
{
	'0','1','2','3','4','5','6','7','8','9',
	'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
	'.','-',' ','\t','\n','\r',';','=','+','#'
};

/*
	S - space,
	T - tab,
	E - enter,
	C - clear line
*/
const int FiniteStateMachine::stateMatrix[NUM_STATE][NUM_OF_CHARACTERS] =
{
				// 0 1 2 3 4 5 6 7 8 9 a b c d e f g h i j k l m n o p q r s t u v w x y z . - S T E C ; = + #
	/* state 0 */ {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},		// IDLE_STATE
	
	/* state 1 */ {J,J,J,J,J,J,J,J,J,J,C,C,C,C,8,C,C,C,2,C,C,C,C,C,C,C,C,C,C,4,C,C,C,C,C,C,K,D,H,H,H,H,G,F,E,M},		// START_STATE

	/* state 2 */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,3,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},
	/* state 3 */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},		// T_IF

				// 0 1 2 3 4 5 6 7 8 9 a b c d e f g h i j k l m n o p q r s t u v w x y z . - S T E C ; = + #
	/* state 4 */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,5,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},
	/* state 5 */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,6,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},
	/* state 6 */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,7,C,C,C,C,C,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},
	/* state 7 */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},		// T_THEN

				// 0 1 2 3 4 5 6 7 8 9 a b c d e f g h i j k l m n o p q r s t u v w x y z . - S T E C ; = + #
	/* state 8 */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,9,C,C,C,C,C,C,C,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},
	/* state 9 */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,A,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},
	/* state A */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,B,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},
	/* state B */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},		// T_ELSE

	/* state C */ {C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,I,0,0,0,0,0,0,0,0,0},		// T_ID

	/* state D */ {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,I,0,0,0,0,0,0,0,0},		// T_MINUS

	/* state E */ {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,I,0},		// T_PLUS

	/* state F */ {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},		// T_EQ

	/* state G */ {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},		// T_SEMI

	/* state H */ {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,H,H,H,H,0,0,0,0},		// T_WHITE_SPACE

	/* state I */ {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},		// T_ERROR

	/* state J */ {J,J,J,J,J,J,J,J,J,J,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,K,0,0,0,0,0,0,0,0,0},		// T_NUM

	/* state K */ {K,K,K,K,K,K,K,K,K,K,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,0,0,0,0,0,0,0,0,0},		// T_REAL

	/* state M */ {M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,M,0,0,M,M,M,M},		// T_COMMENT

};


TokenType FiniteStateMachine::getTokenType(int stateNumber)
{
	return stateToTokenTable[stateNumber];
}


void FiniteStateMachine::initStateMachine()
{
	for (int i = 0; i < NUM_STATE; i++)
	{
		map<char, int> stateTransitions;
		for (int j = 0; j < NUM_OF_CHARACTERS; j++)
		{
			stateTransitions[supportedCharacters[j]] = stateMatrix[i][j];
		}
		stateMachine[i] = stateTransitions;
	}
}


int FiniteStateMachine::getNextState(int currentState, char transitionLetter)
{
	STATE_MACHINE::iterator it = stateMachine.find(currentState);
	
	if (it == stateMachine.end())
	{
		string strCurrentState;
		stringstream ss;
		ss << currentState;
		ss >> strCurrentState;
		string errMessage = "\nEXCEPTION: currentState = " + strCurrentState + " is not a valid state!";
		throw runtime_error(errMessage.c_str());
	}

	map<char, int>::iterator cit = it->second.find(transitionLetter);

	if (cit == it->second.end())
	{
		return INVALID_STATE;
	}
	else
	{
		return cit->second;
	}
}

