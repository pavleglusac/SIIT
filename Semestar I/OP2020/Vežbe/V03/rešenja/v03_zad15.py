if __name__ == '__main__':
    n = int(input("Unesite koliko prvih prirodnih brojeva želite da saberete:"))
    sum = 0

    for number in range(1, n+1):
        sum += number

    print("Suma prvih", n, "prirodnih brojeva iznosi", sum)
