def greatest_common_divisor(first, second):

    while True:
        if first > second:
            first = first % second
        else:
            second = second % first
        if first == 0:
            return second
        elif second == 0:
            return first


if __name__ == '__main__':
    print(greatest_common_divisor(3, 15))