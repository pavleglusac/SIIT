from math import pi

if __name__ == '__main__':
    poluprecnik = eval(input("Unesite poluprečnik: "))
    V = 4/3*pi*poluprecnik**3
    print("Zapremina sfere je", V)
    A = 4*pi*poluprecnik**2
    print("Površina sfere je", A)