### Ovde će se nalaziti svi rešeni zadaci sa vežbi, kao i informacije o datumima kolokvijuma, pdf-ovi i ostali materijali.

# Diskretna Matematika

* **[Snimci i zadaci sa vežbi](https://sites.google.com/site/radojkaciganovicftn/nastava/diskretna-matematika/ve%C5%BEbe)**
* **[Materijali sa predavanja](https://sites.google.com/view/jovanka-pantovic/dm)**
* **[Zbirka zadataka](https://drive.google.com/file/d/1cLuVIls6oagrPt_TguadOZUecNpFC259/view)**

* **[PRIJAVA ZA KOLOKVIJUM 1](https://docs.google.com/spreadsheets/d/1EaKMWu4nKmnwkLckb1-ibKfhX8vld4LCgiwaBjkHxNQ/edit?fbclid=IwAR3_QWWgOImf2b7wW9kMpKGy5pUsyqvNWq2OvHsDj4PGVVAH8nDxdJfI6sA#gid=0)**

| Obaveza | Datum | Vreme | Bodovi |
| ------------- | :-------------: | :-------------: | :-------------: |
| **Kolokvijum 1** (kombinatorika) | **20.12.2020.** | **11:30** | **45** |
| - teorijski test 1 | ^ | ^ | 15 |
| - pismeni deo 1  | ^ | ^ | 30 |
| **Kolokvijum 2** (grafovi) | **25.1.2021.** | TBD | **45** |
| - teorijski test 2 | ^ | ^ | 15 |
| - pismeni deo 2  | ^ | ^ | 30 |

<br></br>
#### Predispitne obaveze (teorijski testovi)
 - **Minimum je 9 bodova** (ukupno valjda). Predispitne obaveze se mogu raditi samo na kolokvijumima.
#### Pismeni delovi
 -  Minimum za prolaz je **po 15 bodova na svakom**.
 - _**Kolokvijum 1**_
   - Navodno neće biti zadaci iz generatornih funkcija i (navodno) neće biti da sami pravimo rekurentne relacije (Ono tipa skup {0, 1, 2}, broj reči dužine n koje sadrže paran broj samoubilačkih tendencija)
   - **[Rezultati 20.12.](https://drive.google.com/file/d/1xQYuEDaq4vrNeaCvRQ26L7z5J94OZVSw/view)**
 - _**Kolokvijum 2**_
#### Usmeni deo (obavezan)
 -  **5+5 bodova, minimum 3+3 za prolaz.**
 - Takođe se radi na kolokvijumu

# Organizacija Podataka

* **[Materijali sa predavanja](http://www.acs.uns.ac.rs/sr/node/237/1967797)**
* **[Materijali sa vežbi](http://www.acs.uns.ac.rs/sr/node/237/1967798)**

| Obaveza | Datum | Vreme | Bodovi |
| ------------- | :-------------: | :-------------: | :-------------: |
| **Kolokvijum 1** | ~~6.12.2020.~~ 17.1.2021. | TBD | **30** |
| **Kolokvijum 2** | 17.1.2021. | TBD | **40** |

#### 1. Kolokvijum - ~~6.12.2020.~~ 17.1.2021.
 * **Sve je valjda 17.1.**
 * provere teorijskih znanja i znanja o odabranim datotečkim formatima
 * (treba mi potvrda za ovu informaciju) za teorijski deo dolaze u obzir prve 3 prezentacije. Pitanja najvise na zaokruzivanje, ali neka su i standardnog tipa. 10-15 pitanja.
#### 2. Kolokvijum - 17.1.2021.
 * provere znanja o serijskoj i sekvencijalnoj organizaciji datoteka i o rasutoj i indeks-sekvencijalnoj organizaciji datoteka
#### 

# Nelinearno programiranje i evolutivni algoritmi
* **[Snimci predavanja](https://www.youtube.com/playlist?list=PLejOgNVBWnrO6WE9D-xa-q_pQgWVcwyDk)**

* **[Snimci vežbi](https://www.youtube.com/playlist?list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs)**

* **[Prijava za kolokvijume](https://docs.google.com/spreadsheets/d/1F-BKuGF947x8hwTqNItalDHDNEZfLJfRUbqrSvIyBPk/edit#gid=0)**

| Obaveza | Datum | Vreme | Bodovi |
| ------------- | :-------------: | :-------------: | :-------------: |
| **Računarski kolokvijum 1** | 13.12.2020. | 9:00 - 15:00 | **20** |
| **Teorijski kolokvijum** | 23.12.2020. | 18:00 - 20:00 | **60** |
| (ili) **Računarski kolokvijum 2** | Januar 2021. | TBD | _**20**_ |
| (ili) **Projekat** | do 15.01.2021. | TBD | _**25**_ |

#### 1. Računarski kolokvijum 1 - 13.12.2020.
 - jednodimenzionalne numeričke metode i prvo predavanje profesora Rapaića (od 13. Novembra 2020)
 - **2 zadatka**, jedan iz jednodimenzionalnih, jedan iz višedimenzionalnih metoda.
 - **teorijska pitanja**
 - Predavanja: [P08](https://www.youtube.com/watch?v=oLobVRDP9yk&list=PLejOgNVBWnrO6WE9D-xa-q_pQgWVcwyDk&index=9&ab_channel=AutomatikaFTN) [P09](https://www.youtube.com/watch?v=1TBFPxEH9tQ&list=PLejOgNVBWnrO6WE9D-xa-q_pQgWVcwyDk&index=10&ab_channel=AutomatikaFTN) [P10](https://www.youtube.com/watch?v=ec3W3Drevvo&list=PLejOgNVBWnrO6WE9D-xa-q_pQgWVcwyDk&index=11&ab_channel=AutomatikaFTN)
 - Vežbe: [RV02](https://www.youtube.com/watch?v=5BbwB8cTmM0&list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs&index=11&ab_channel=Automat)
#### 2. Teorijski kolokvijum - 23.12.2020.
 - Vežbe: [V01](https://www.youtube.com/watch?v=lFOasycZKWA&list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs&index=1&ab_channel=AutomatikaFTN) [V02](https://www.youtube.com/watch?v=f8bswSXxnKQ&list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs&index=2&ab_channel=AutomatikaFTN) [V03](https://www.youtube.com/watch?v=ZJIHkrfb2aM&list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs&index=3&t=2824s&ab_channel=AutomatikaFTN) [V04](https://www.youtube.com/watch?v=sTEey2Y36lI&list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs&index=4&ab_channel=AutomatikaFTN) [V05](https://www.youtube.com/watch?v=-gW2hqA5dpg&list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs&index=5&ab_channel=AutomatikaFTN) [V06](https://www.youtube.com/watch?v=OgnGlRQXl1I&list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs&index=6&ab_channel=AutomatikaFTN) [V07](https://www.youtube.com/watch?v=UzIwEPdL1RU&list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs&index=7&ab_channel=AutomatikaFTN) [V10](https://www.youtube.com/watch?v=BzxDhQ5s3qg&list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs&index=10&ab_channel=AutomatikaFTN) [Priprema za kolokvijum](https://www.youtube.com/watch?v=011zMatOrGU&list=PLejOgNVBWnrOaS6VXcZh_yQwXfPqN1VWs&index=9&t=1968s&ab_channel=AutomatikaFTN)
 - Nema teorije na ovom pismenom
 - Ako položis ovo nema usmenog {samo za ovaj rok važi}
##### 3.1 Računarski kolokvijum 2 - Januar 2021.
##### 3.2 Projekat - do 15.01.2021.
##### 4. **Usmeni (obavezan)** [potvrđuje ocenu]

```mermaid
graph TD

  A[Rač. klk. 1 >> 20b] --> B[Teorijski klk. >> 60b]
  B --> C[Rač. klk. 2 >> 20b]
  B --> D[Projekat >> 25b]
  C --> E[Ukupno >> 100b]
  D --> F[Ukupno >> 105b]
```

# Objektno orijentisano programiranje 2
* **[Knjiga](http://f.javier.io/rep/books/Programming_%20Principles%20and%20Pra%20-%20Bjarne%20Stroustrup.pdf)**
* **[Materijali i osnovne informacije o polaganju](https://www.rt-rk.uns.ac.rs/?q=predmeti/siit/oop-2-objektno-orijentisano)**

* **[Pudi Pudi Giga Pudding (Original Remix)](https://www.youtube.com/watch?v=tIGIClyiUMI&ab_channel=AnimeApproved)**

| Obaveza | Datum | Vreme | Bodovi |
| ------------- | :-------------: | :-------------: | :-------------: |
| **Pohađanje vežbi** | null  | null | **5** |
| **Domaći zadatak** | do 11.11.2020. | 23:59 | **5** |
| **Projekat** | do 16.1.2021. | 23:59 | **40** |
| **Ispit iz teorije** | TBD | TBD | **50** |

##### Ispit iz teorije je obično pismeni.

<br></br>
# Numerički algoritmi i numerički softver

| Obaveza | Datum | Vreme | Bodovi | Minimalno za prolaz
| ------------- | :-------------: | :-------------: | :-------------: | :-------------: |
| **Teorija** | **↓** | **↓** | **45** | **26** |
| - deo 1| TBD | TBD | 45/2 | 50% |
| - deo 2  | TBD | TBD | 45/2 | 50% |
| **Predispitne** | **↓** | **↓** | **50 ili 65** |
| - kolokvijum 1 | **\*** 12.12.2020. | 8:00 - 13:00 | 27 | 14 |
| - **(ili)** kolokvijum 2  | TBD | TBD | 23 | 11 |
| - **(ili)** projekat  | 17.2-19.2.2021. | TBD | 38 | uspešna odbrana |

#### 1. Kolokvijum - 12.12.2020.
 - Zadaci će biti postavljeni na MS Teams u **sredu 9.12.2020.**
 - Mi ih radimo kod kuće i **branimo u subotu 12.12.2020.** u periodu od 8 do 13 časova.
#### Projekat
 - Na MS Teams platformi, u fajlovima predmeta NANS, nalaze se **osnovne informacije o izradi projekta**, kao i primer dobrog predloga i prezentacije
 - Potrebno je poslati predlog projekta najkasnije do **petka 6.12.2020. u 23:59**
 - _**Obavezno** pročitati postavljene informacije na Teams-u, jer nije moguce sve uopštiti ovde_
#### Teorija
 - Svi koji su zadovoljni ostvarenim bodovima na predispitnim obavezama se mogu prijaviti da bez polaganja teorije dobiju ocenu. 
 - _**Za sve informacije pogledajte obaveštenje na Timsu.**_ 
 - [**LINK ZA POTVRDU UPISA OCENE BEZ TEORIJE**](https://forms.gle/9UegS1cqDdubfSH79)
 - **All praise Aleksandar!**

```mermaid
graph TD

  A[Teorija >> 45b] --> B[Kolokvijum 1 >> 27b]
  B --> C[Kolokvijum 2 >> 23b]
  B --> D[Projekat >> 38b]
  C --> E[Ukupno >> 95b]
  D --> F[Ukupno >> 110b]
```

<br></br>
