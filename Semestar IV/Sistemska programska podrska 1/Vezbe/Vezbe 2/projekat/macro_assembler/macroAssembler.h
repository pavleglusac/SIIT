#ifndef __MACRO_ASSEMBLER_H__
#define __MACRO_ASSEMBLER_H__

#include "macroAssemblerLib.h"

bool macroAssembler(std::string input_file, std::string output_file);

#endif /*__MACRO_ASSEMBLER_H__*/
