#pragma once

#include <string>

/**
 * State symbol definitions
 */
#define IDLE_STATE				0
#define START_STATE				1
#define INVALID_STATE			(-2)

/**
 * Use these definitions for easily addressing "human type" matrix
 */
#define A 10
#define B 11
#define C 12
#define D 13
#define E 14
#define F 15
#define G 16
#define H 17
#define I 18
#define J 19
#define K 20
#define M 21
#define N 22
#define O 23
#define P 24
#define R 25

/**
 * Number of states in FSM
 */
#define NUM_STATE				(R+1)

/**
 * Number of supported characters
 */
#define NUM_OF_CHARACTERS		46

/**
* Supported token types.
*/
enum TokenType
{
	T_NO_TYPE,
	T_COMMENT,
	T_ID,
	T_NUM,
	T_REAL,
	T_IF,
	T_THEN,
	T_ELSE,
	T_PLUS,
	T_MINUS,
	T_EQ,
	T_SEMI,
	T_WHITE_SPACE,
	T_ERROR,
	T_EQEQ,
	T_DEC,
	T_END_OF_FILE
};

/**
 * Alignment definitions for nice printing
 */
#define LEFT_ALIGN				20
#define RIGHT_ALIGN				25
