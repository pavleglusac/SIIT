################################################################################
#  Odsek za racunarsku tehniku i medjuracunarske komunikacije                  #
################################################################################
#                                                                              #
# Naziv Modula: fact.s                                                         #
#                                                                              #
# Opis: izracunavanje faktorijela vrednosti smestene na lokaciji value         #
#                                                                              #
################################################################################

#data section
.org data

value: .word 9

#text section
.org text

.macro multlo($arg0,$arg1)
	mult  $arg0, $arg1
	MFLO  $arg0
.end_macro
   
.macro multhi($arg0,$arg1)
	mult  $arg0, $arg1
	MFHI  $arg0
.end_macro


main:
	lui		$t1, HI(ADDR(value))     	# ucitavanje adrese
	ori		$t1, $t1, LO(ADDR(value))    			
	lw		$t2, 0($t1)					# ucitavanje vrednosti faktorijela sa adrese u $t1

	add		$t3, $t2, $0   # postavljanje rezultata na value
	addi	$t1, $0, 1
	
	sub		$t2, $t2, $t1  # umanjivanje brojaca

	blez	$t2, exit
	nop

loop:
	multlo	$t3, $t2

	sub		$t2, $t2, $t1  # umanjivanje brojaca

	multhi	$t3, $t2

	blez	$t2, exit
	nop

	beq		$0, $0, loop

exit:
	nop