#ifndef __MACRO_ASSEMBLER_LIB_H__
#define __MACRO_ASSEMBLER_LIB_H__

#include "assemblerLib.h"

#define D_MACRO_START       "macro"
#define D_MACRO_END         "end_macro"

#define D_MACRO_NAME_SEP    "("


/**
 * @struct MACRO_DEF
 *
 * @brief structure for macro definitions table element
 */
struct MACRO_DEF {
	std::list<std::string> definition;
    PARAM_STACK fictiveParams;
};

/**
 * macro definisions table map type
 */
typedef std::map<short, MACRO_DEF> TMD_MAP;


/**
 * @brief   Extracts macro definition name and all fictive parameters of macro
 *          definition from the current sourceline.
 *
 *          Only parameters begining with "(" and ending with ")" should be passed.
 *          Error reporting for bad macro definition name or for bad parameters list
 *
 * @param[out]   macroName   In this variable extracted macrodefition name will be returned
 * @param[out]   params      Pointer to the parameters list (extracted parameters, if any, should be returned in this variable)
 * @param[in]    sourceLine  current source line
 * @param[in]    lineNumber  the number of the current line
 *		
 * @return  true if macro definition name and parameters list ok; false otherwise
 */
bool getMacroNameAndParams(std::string &macroName, PARAM_STACK &params, std::string sourceLine, long lineNumber);

/**
 * @brief   Replace fictive with real parameters in the line of macrodefinition
 *
 * @param[in]   macroDefinitionLine             Original macro definition line
 * @param[out]   macroDefinitionRealParamLine   Macro defintion line with replaced (real) parameters
 * @param[in]   tmd                             Macro definition table with indexed definition
 * @param[in]   index                           Index of macro definition for searching fictive parameters
 * @param[in]   realParams                      Real parameters for replacing fictive ones
 */
void fictiveToRealParams(std::string macroDefinitionLine, std::string &macroDefinitionRealParamLine, TMD_MAP tmd, short index, PARAM_STACK realParams);

#endif /*__MACRO_ASSEMBLER_LIB_H__*/