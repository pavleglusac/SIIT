from math import sqrt

def quadratic_equation(a, b, c):
    D = b**2-4*a*c
    if D>0:
        square_root = sqrt(D)
        x1 = (-b+square_root)/(2*a)
        x2 = (-b-square_root)/(2*a)

        return x1, x2


if __name__ == '__main__':
    solution = quadratic_equation(-1,5,5)

    # Poredimo solution sa None zbog jasnoće
    # Vrednost None se automatski evaluira u False, tako da smo mogli i da napišemo if not solution:
    if solution == None:
        print("Nema realnih rešenja.")
    else:
        print("Rešenja kvadratne jednačine su", solution)
