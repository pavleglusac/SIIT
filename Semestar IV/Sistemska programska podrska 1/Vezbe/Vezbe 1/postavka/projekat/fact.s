################################################################################
#  Odsek za racunarsku tehniku i medjuracunarske komunikacije                  #
################################################################################
#                                                                              #
# Naziv Modula: fact.s                                                         #
#                                                                              #
# Opis: izracunavanje faktorijela vrednosti smestene na lokaciji value         #
#                                                                              #
################################################################################

#data section
.org data

#value for factoriel calculation
value: 		.word 9

#reserve 7 bytes of free space
.space 7

#temporary variable
var:		.word 3

#text section
.org text

main:
	lui		$t1, HI(ADDR(value))     	# ucitavanje adrese
	ori		$t1, $t1, LO(ADDR(value))    			
	lw		$t2, 0($t1)					# ucitavanje vrednosti faktorijela sa adrese u $t1

	add   	$t3, $t2, $0   				# postavljanje rezultata na vrednost value
	addi  	$t1, $0, 1

	sub   	$t2, $t2, $t1  				# umanjivanje brojaca

	blez  	$t2, exit
	nop

loop:
	mult  	$t3, $t2
	MFLO  	$t3

	sub   	$t2, $t2, $t1  				# umanjivanje brojaca

	blez  	$t2, exit
	nop

	beq 	$0, $0, loop

exit:
	nop