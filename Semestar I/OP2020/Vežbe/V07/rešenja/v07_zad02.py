def number_of_years(interest_rate):
    counter = 0
    current_amount = 1
    while current_amount < 2:
        counter += 1
        current_amount = (1+interest_rate) * current_amount

    return counter

if __name__ == '__main__':
    print(number_of_years(0.04))