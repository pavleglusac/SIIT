package polimorfizam;

public class Automobil implements Vozilo {
	public Automobil() {
		System.out.println("Konstruktor Automobila");
	}

	public Automobil(String s) {
		System.out.println("Konstruktor Automobila sa imenom: " + s);
	}

	@Override
	public double platiPutarinu() {
		return 240;
	}

}
