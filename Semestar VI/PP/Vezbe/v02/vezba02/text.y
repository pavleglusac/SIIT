%{
  #include <stdio.h>
  int yylex(void);
  int yyparse(void);
  int yyerror(char *);
  extern int yylineno;
  int brOB = 0;
  int brUP = 0;
  int brUZ = 0;
  int paragraph_counter = 0;

%}

%token  DOT
%token  CAPITAL_WORD
%token  WORD
%token  UZVIK
%token  UPIT
%token  ZAREZ
%token  NL
%%

text
: paragraph NL
  { paragraph_counter++; }
| text paragraph NL
  { paragraph_counter++; }
;

paragraph
: sentence
| paragraph sentence
;     

sentence 
  : words DOT {brOB++;}
  | words UPIT {brUP++;}
  | words UZVIK {brUZ++;}
  ;

words 
  : CAPITAL_WORD
  | words mozda_zarez WORD
  | words mozda_zarez CAPITAL_WORD    
  ;

mozda_zarez
  : ZAREZ
  |
  ;
%%

int main() {
  yyparse();
  printf("OB: %d\nUp: %d\nUz %d\nBroj paragrafa %d\n",brOB,brUP,brUZ,paragraph_counter);
  

}

int yyerror(char *s) {
  fprintf(stderr, "line %d: SYNTAX ERROR %s\n", yylineno, s);
} 

