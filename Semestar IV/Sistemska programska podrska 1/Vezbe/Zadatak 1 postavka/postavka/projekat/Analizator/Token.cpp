#include <iostream>
#include <iomanip>

#include "Token.h"
#include "FiniteStateMachine.h"

using namespace std;


Token::Token(void)
{
	this->tokenType = T_NO_TYPE;
	this->value = "";
}


Token::~Token(void)
{
}


TokenType Token::getType()
{
	return this->tokenType;
}


void Token::setType(TokenType t)
{
	this->tokenType = t;
}


string Token::getValue()
{
	return this->value;
}


void Token::setValue(string s)
{
	this->value = s;
}


void Token::makeToken(int begin, int end, std::vector<char>& programBuffer,  int lastFiniteState)
{
	string value = "";
	for (int i = begin; i < end; i++)
	{
		value += programBuffer[i];
	}
	this->value = value;
	this->tokenType = FiniteStateMachine::getTokenType(lastFiniteState);
}


void Token::makeErrorToken(int pos, std::vector<char>& programBuffer)
{
	this->tokenType = T_ERROR;
	this->value = programBuffer[pos];
}


void Token::makeEofToken()
{
	this->tokenType = T_END_OF_FILE;
	this->value = "EOF";
}


void Token::printTokenInfo()
{
	cout << setw(LEFT_ALIGN) << left << tokenTypeToString(this->tokenType);
	cout << setw(RIGHT_ALIGN) << right << this->value << endl;
}


void Token::printTokenValue()
{
	cout << this->value << endl;
}


string Token::tokenTypeToString(TokenType t)
{
	switch (t)
	{
		case T_NO_TYPE:			return "[T_NO_TYPE]";
		case T_COMMENT:			return "[T_COMMENT]";
		case T_ID:				return "[T_ID]";
		case T_NUM:				return "[T_NUM]";
		case T_REAL:			return "[T_REAL]";
		case T_IF:				return "[T_IF]";
		case T_THEN:			return "[T_THEN]";
		case T_ELSE:			return "[T_ELSE]";
		case T_PLUS:			return "[T_PLUS]";
		case T_MINUS:			return "[T_MINUS]";
		case T_EQ:				return "[T_EQ]";
		case T_SEMI:			return "[T_SEMI]";
		case T_WHITE_SPACE:		return "[T_WHITE_SPACE]";
		case T_ERROR:			return "[T_ERROR]";
		case T_EQEQ:			return "[T_EQEQ]";
		case T_DEC:				return "[T_DEC]";
		case T_END_OF_FILE:		return "[T_END_OF_FILE]";
		default:				return "";
	}
}

