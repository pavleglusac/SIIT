
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
-- Libraries.

entity lprs1_homework1 is
	port(
		i_x   :  in std_logic_vector(3 downto 0);
		i_y   :  in std_logic_vector(3 downto 0);
		i_z   :  in std_logic_vector(1 downto 0);
		i_sel :  in std_logic_vector(1 downto 0);
		o_res : out std_logic_vector(3 downto 0);
		o_cmp : out std_logic_vector(1 downto 0);
		o_enc : out std_logic_vector(1 downto 0)
	);
end entity;


architecture arch of lprs1_homework1 is
	-- Signals.
	
	signal s_shl : std_logic_vector(3 downto 0);
	signal s_shr : std_logic_vector(3 downto 0);

	signal s_add : std_logic_vector(3 downto 0);
	signal s_sub : std_logic_vector(3 downto 0);
	
	signal s_dec : std_logic_vector(3 downto 0);
	
	signal s_const0 : std_logic_vector(3 downto 0);
	signal s_const1 : std_logic_vector(3 downto 0);
	
	signal s_mux : std_logic_vector(3 downto 0);
	
begin
	-- Design.
	
	-- POMERAC (LOGICKO POMERANJE U LEVO 3 BITA)
	
		s_shl <= i_x(0)  &  '0'  &  '0'  &  '0';
	
	
	-- POMERAC (LOGICKO POMERANJE U DESNO 1 BIT)
	
		s_shr <= '0'  &  i_y(3 downto 1);
		
		
	-- SABIRAC
		
		s_add <= s_shl + s_shr;
		
		
	-- DEKODER	
	
		s_dec <= "0001" when i_z = "00" else
					"0010" when i_z = "01" else
					"0100" when i_z = "10" else
					"1000";
					
					
	-- ODUZIMAC
		
		s_sub <= s_dec - i_x;
		
		
	-- KONSTANTNI SIGNALI
	
		s_const0 <= "0111";
		s_const1 <= "1110";
		
		
	-- MULTIPLEKSER
	
		s_mux <= s_add when i_sel = "00" else
					s_const1 when i_sel = "01" else
					s_const0 when i_sel = "10" else
					s_sub;
					
		
	-- IZLAZNI SIGNALI
	
		O_res <= s_mux;
		
		
	-- KOMPARATOR
	
		O_cmp(0) <= '1' when s_mux /= "0000" else '0';
		O_cmp(1) <= '1' when s_mux < "0000" else '0';
	
	
	-- KODER
	
		o_enc <= "11" when s_mux(3)='1' else 
					"10" when s_mux(2)='1' else 
					"01" when s_mux(1)='1' else 
					"00"; 
			
	
end architecture;
