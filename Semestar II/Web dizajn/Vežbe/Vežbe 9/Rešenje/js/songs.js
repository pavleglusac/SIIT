let firebaseURL = "https://wd-vezbe8-default-rtdb.firebaseio.com/songs.json";

let audioPlaylist = document.getElementById('audioPlaylist');
let player = document.getElementById('player');


let request = new XMLHttpRequest();

request.onreadystatechange = function () {
    if (this.readyState == 4) {
        if (this.status == 200) {
            let songs = JSON.parse(this.responseText);

            for (let i = 0; i < songs.length; i++) {
                let song = songs[i];

                // Za svaku pesmu kreiramo po jedan <li> element
                let songLi = document.createElement('li');

                // kreiramo <span> u koji cemo upisati redni broj pesme
                let numberSpan = document.createElement('span');
                // dodajemo CSS klasu namenjenu ovim elementima
                numberSpan.classList.add('orderNumber');
                // Redni brojevi u listi krecu od 1, pa zbog toga uvecavamo i
                numberSpan.innerText = i + 1;
                // Kreiran, popunjen i stilizovan <span> dodajemo u <li>
                songLi.appendChild(numberSpan);

                // Kreiramo <span> u koji cemo upisati izvodjaca pesme
                let artistSpan = document.createElement('span');
                artistSpan.classList.add('artistSpan');
                // Podatak o izvodjacu se nalazi u 'artist' atributu svake pesme (videti firebase-data.json fajl)
                artistSpan.innerText = song.artist;
                songLi.appendChild(artistSpan);

                // Kreiramo <span> za naslov pesme
                let titleSpan = document.createElement('span');
                titleSpan.classList.add('songSpan');
                titleSpan.innerText = song.title;
                songLi.appendChild(titleSpan);

                // U data atribut 'data-url' upisemo link do mp3 fajla. Ovo cemo koristiti za pustanje pesme na klik
                // Vise o data atributima: https://www.w3schools.com/tags/att_global_data.asp
                songLi.setAttribute('data-url', song.link);

                // Klik na kreirane <li> elemente ucitava pesmu u <audio> tag i pusta je.
                songLi.addEventListener('click', function (e) {
                    // Izvodjaca pesme procitamo tako sto nadjemo odgoletajuci element u koji je zapisan
                    // i procitamo njegov tekstualni sadrzaj.
                    let artist = this.querySelector('.artistSpan').innerText;
                    // Naziv izvodjaca upisemo u <span> sa id-jem 'titleElement' koji se nalazi iznad <audio> elementa
                    let playerSongArtist = document.getElementById('artistElement');
                    playerSongArtist.innerText = artist;

                    // Naslov pesme procitamo tako sto nadjemo odgoletajuci element u koji je zapisan
                    // i procitamo njegov tekstualni sadrzaj.
                    let title = this.querySelector('.songSpan').innerText;
                    // Naslov pesme upisemo u <span> sa id-jem 'titleElement' koji se nalazi iznad <audio> elementa
                    let playerSongTitle = document.getElementById('titleElement');
                    playerSongTitle.innerText = title;

                    // Procitamo link do fajla iz data atributa koji smo kreirali gore.
                    let url = this.getAttribute('data-url');
                    // Procitani link postavimo kao 'src' atribut <audio> taga
                    player.setAttribute('src', url);


                    // Pusanje pesme se izvodi pozivanjem funkcije 'play()' audio elementa
                    player.play();
                });

                audioPlaylist.appendChild(songLi);
            }
        } else {
            alert('Greska prilikom ucitavanja pesama.')
        }
    }
}

request.open('GET', firebaseURL);
request.send();