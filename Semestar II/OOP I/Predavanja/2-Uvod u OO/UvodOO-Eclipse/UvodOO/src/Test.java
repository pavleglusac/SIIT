import java.util.HashMap;

import automobili.Automobil;

public class Test {

	public static final int PON = 1;
	public static final int UTO = 2;
	
	enum Dani {
		PON, UTO, SRE, CET, PET, SUB, NED
	};
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Tacka t = new Tacka(0.0, 1.0);
		Tacka t2 = new Tacka(0.0, 2.0);
		
		System.out.println(t.equals(t2));
		
//		Tacka centarKruga = new Tacka(); // 0,0
//		Krug k = new Krug(centarKruga, 5.0);
		
		Krug k = new Krug(0.0, 0.0, 5.0);
		
		System.out.println(t.equals(k));

		k.proveri(t);
		
		Krug k2 = new Krug(0.0, 0.0, 5.0);
		
		System.out.println(k.equals(k2));

		HashMap<String, Automobil> mapa = new HashMap<String, Automobil>();
		mapa.put("NS 123-XY", new Automobil());
		mapa.put("NS 321-�W", new Automobil());
		
		System.out.println(mapa.get("NS 321-�V"));
	}

}
