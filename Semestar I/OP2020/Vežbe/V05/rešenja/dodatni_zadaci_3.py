def digitron(num1, num2, operator):

    # Proveravamo da li je prvi operand broj
    if type(num1) != int and type(num1) != float:
        print("Prvi operand nije broj.")
        return None

    # Proveravamo da li je drugi operand broj
    if type(num2) != int and type(num2) != float:
        print("Drugi operand nije broj.")
        return None

    if operator == "+":
        return num1+num2

    if operator == "-":
        return num1-num2

    if operator == "*":
        return num1 * num2

    if operator == "/":
        if num2 == 0:
            print("Deljenje nulom nije dozvoljeno.")
            return None
        return num1 / num2

    print("Operacija", operator, "nije podržana.")
    return None


if __name__ == '__main__':
    result = digitron(11.11, 5, "-")
    if result:
        print("Rezultat je", result)