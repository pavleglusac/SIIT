package polimorfizam;


public class Test {

	public static void main(String[] args)  {
//		Vozilo v = new Vozilo();
		Automobil a = new Automobil("BMW");
		Kamion k = new Kamion("FAP");
		NaplatnaRampa nr = new NaplatnaRampa();
		double iznos = nr.naplatiPutarinu(a);
		iznos += nr.naplatiPutarinu(k);
//		iznos += nr.naplatiPutarinu(v);

		System.out.println("Pokupio na naplatnoj rampi: " + iznos);
	}

}
