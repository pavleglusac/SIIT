def reading(file_path, delimiter):
    file = open(file_path, "r")
    lines = file.readlines()
    output = []
    for line in lines:
        # brišemo \n karakter
        line = line.replace("\n", "")
        line_content = line.split(delimiter)
        output.append(line_content)

    file.close()
    return output

if __name__ == '__main__':
    print("Zadatak 3:\n")
    print(reading("../fajlovi/korisnici.txt", "|"))