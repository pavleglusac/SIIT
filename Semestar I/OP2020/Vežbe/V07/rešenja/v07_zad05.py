from v07_zad04 import is_prime

def find_all_primes(x):
    result = []
    for current_number in range(1, x):
        if is_prime(current_number):
            result.append(current_number)

    return result

print(find_all_primes(20))