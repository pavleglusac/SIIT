
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

-- Libraries.

entity lprs1_homework2 is
	port(
		i_clk    :  in std_logic;
		i_rst    :  in std_logic;
		i_run    :  in std_logic;
		i_pause  :  in std_logic;
		o_digit0 : out std_logic_vector(3 downto 0);
		o_digit1 : out std_logic_vector(3 downto 0);
		o_digit2 : out std_logic_vector(3 downto 0);
		o_digit3 : out std_logic_vector(3 downto 0)
	);
end entity;


architecture arch of lprs1_homework2 is

	-- Signals.
	
	signal s_en_1us 	: std_logic;
	signal s_en0 		: std_logic;
	signal s_en1 		: std_logic;
	
	signal s_tc_1us 	: std_logic;
	signal s_tc0      : std_logic;
	signal s_tc1      : std_logic;
	
	signal s_cnt_1us 	: std_logic_vector(5 downto 0);							-- 39 DEC  =  100111 BIN -> KORISTITI 6 CIFARA
	signal s_cnt0 		: std_logic_vector(3 downto 0);
	signal s_cnt1 		: std_logic_vector(3 downto 0);
	
	
	
	
begin
	-- Body.
	
	-- JOVAN SRDANOV RA 145/2019
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	-- KONTROLA DOZVOLE BROJANJA
	
	process (i_clk, i_rst) is begin
	
			if (i_rst = '1') then
			
				s_en_1us <= '0';
				
			elsif (i_clk'event and i_clk = '1') then
			
				if ( i_run='1' or (i_run = '0' and  i_pause = '0' and  s_en_1us = '1') ) then
				
				s_en_1us <= '1';
				
				else 
				
				s_en_1us <= '0';
				
				end if;
				
			end if;
			
	end process;
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	-- BROJAC JEDNE us
	
	-- 40-1=39
	
	process (i_clk, i_rst) begin 	
	
			if(i_rst = '1') then
			
				s_cnt_1us <= ( OTHERS => '0' );
				
			elsif (i_clk'event and i_clk='1') then
			
				if (s_en_1us = '1') then
				
					s_cnt_1us <= "000001" + s_cnt_1us;
					
					if (s_cnt_1us = "100111" ) then 										-- 39 DEC  =  100111 BIN
					
						s_cnt_1us <= ( OTHERS => '0' );
					
					end if;
					
				end if;
				
			end if;
			
	end process;
	
	s_tc_1us <= '1' when s_cnt_1us = "100111" else '0'; 							-- 39 DEC  =  100111 BIN
	
	s_en0 <= '1' when (s_en_1us= '1' and s_tc_1us= '1') else '0';
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	-- BROJAC NULTE CIFRE
	
	-- 10-1=9
	
	process (i_clk, i_rst) begin 
	
			if(i_rst = '1') then
			
				s_cnt0 <= ( OTHERS => '0' );
				
			elsif (i_clk'event and i_clk='1') then
			
				if (s_en0 = '1') then
				
					s_cnt0 <= "0001" + s_cnt0; 
						
					if (s_cnt0 = "1001") then							 					-- 9 DEC  =  1001 BIN
					
						s_cnt0 <= ( OTHERS => '0' );
				
					end if;
				
				end if;
				
			end if;
			
	end process;
	
	s_tc0 <= '1' when s_cnt0 = "1001" else '0'; 					
	
	s_en1 <= '1' when (s_en0= '1' and s_tc0= '1') else '0';						-- 9 DEC  =  1001 BIN
				

	--------------------------------------------------------------------------------------------------------------------------------
	
	-- BROJAC PRVE CIFRE
	
	-- 5-1=4
	
	process (i_clk, i_rst) begin 
	
			if(i_rst = '1') then
			
					s_cnt1 <= ( OTHERS => '0' );
					
			elsif (i_clk'event and i_clk='1') then
			
				if (s_en1 = '1') then
				
					s_cnt1 <= "0001" + s_cnt1; 
					
					if (s_cnt1 = "0100") then 												-- 4 DEC  =  0100 BIN
					
						s_cnt1 <= ( OTHERS => '0' );
						
					end if;
					
				end if;
				
			end if;
			
	end process;
	
	s_tc1 <= '1' when s_cnt1 = "0100" else '0'; 										-- 4 DEC  =  0100 BIN
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	-- IZLAZI
				
	o_digit0 <= s_cnt0;
	
	o_digit1 <= s_cnt1;
	
	o_digit2 <= "0010";																		-- 0010 BIN  - >  2 DEC
		
	o_digit3 <= "1010";																		-- 1010 BIN  - >  10 DEC
	
	
	
end architecture;
