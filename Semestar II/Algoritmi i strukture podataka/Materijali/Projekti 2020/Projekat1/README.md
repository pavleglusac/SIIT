Napisati program za igranje igre [mice](https://en.wikipedia.org/wiki/Nine_men%27s_morris). Ovu igru igraju 2 igrača (npr. W i B, white i black, beli i crni) sa po 9 figura na tabli sa 24 moguće pozicije za figure. Figure na tabli mogu da stoje samo u presecima datih linija, a kretanje figura na tabli je moguće samo po datim linijama. Igru gubi onaj igrač koji ostane sa 2 figure na tabli ili nije u mogućnosti da odigra svoj potez jer su sva susedna polja zauzeta. Igra se može završiti i u slučaju kada oba igrača spadnu na mali broj figura (npr. oba igrača imaju po 3 figure) jer je razrešenje u tom slučaju skoro nemoguće. Igra se sastoji iz 3 faze:

**Faza 1 - postavljanje figura.** Igra počinje sa praznom tablom. Igrači naizmenično postavljaju svojih 9 figura na slobodna polja na tabli. Ukoliko igrač formira niz od 3 figure (eng. _mill_), ima pravo da ukloni jednu protivničku figuru. Protivnička figura koja se uklanja ne sme biti deo protivničkog niza od 3 figure, osim u slučaju da su sve protivničke figure u nizu od 3.

**Faza 2 - kretanje.** Igrači naizmenično igraju po jedan potez. Potez predstavlja pomeranje svoje figure na susedno slobodno mesto. Cilj kretanja je da se formira niz od 3 svoje figure kada je moguće ukloniti jednu protivničku figuru. Kao i u prvoj fazi, protivnička figura koja se uklanja ne sme biti deo protivničkog niza od 3 figure, osim u slučaju da su sve protivničke figure u nizu od 3. Kada jedan od igrača ostane sa 3 figure na tabli, prelazi se na fazu 3.

**Faza 3 - preskakanje.** Figure se mogu pomeriti na bilo koju slobodnu poziciju na tabli, uključujući i preskakanje.

Obavezna je upotreba stabla i heš mape.

Obavezno je implementirati minimax algoritam sa alfa i beta rezovima.

U svakoj od faza, potez mora da se odigra za manje od 3 sekunde.

Omogućiti poređenje kvaliteta i performansi rešenja sa drugima - nadmetanje programa. Da bismo to ostvarili, potrebno je ulaze i izlaze programa prilagoditi uslovima sa [linka](https://www.hackerrank.com/challenges/morrisnine/problem).

Korisni linkovi:

*   https://en.wikipedia.org/wiki/Alpha–beta\_pruning
*   https://kartikkukreja.wordpress.com/2014/06/29/alphabetasearch/
*   https://kartikkukreja.wordpress.com/2014/03/17/heuristicevaluation-function-for-nine-mens-morris/
*   [https://www.youtube.com/watch?v=xBXHtz4Gbdo](https://www.youtube.com/watch?v=xBXHtz4Gbdo)[![](/images/play_overlay.png)](https://www.youtube.com/watch?v=xBXHtz4Gbdo)
*   Nadmetanje programa:  [https://www.hackerrank.com/challenges/morrisnine/problem](https://www.hackerrank.com/challenges/morrisnine/problem)

### **Nacrt bodovanja**

*   Dva režima rada:
    *   Implementacija za hackerrank: 5 poena
    *   Čovek protiv računara: 5 poena  
          
        
*   Implementacija po fazama:
    *   Faza 1: 5 poena
    *   Faza 2: 5 poena
    *   Faza 3: 2 poena  
          
        
*    Upotreba struktura podataka:
    *   Hash mape: 2 poena
    *   Stabla: 2 poena  
          
        
*   Implementacija heuristika: 4 poena  
      
    
*    Varijabilna dubina: 2 poena  
      
    

Odstupanja od zadatog:

*   Ukoliko student implementira proizvoljan algoritam za odabir poteza, maksimalno može osvojiti 60% poena.
*   Ukoliko student implementira minimax algoritam bez alfa/beta rezova, maksimalno može osvojiti 80% poena.

Performanse:

*   \>5 s po potezu - maksimalno 80% poena
*   \>10 s po potezu - maksimalno 60% poena

Implementacija minimax algoritma bez alfa/beta rezova gde se sledeći potez određuje za više od 10s može da osvoji maksimalno 48% poena (0.8\*0.6 = 0.48).

Opšte informacije o slanju i upload-u zadatka:

*   Zadatak nosi 30 poena.
*   Smestiti sve fajlove zadatka u **folder** pod nazivom projekat1\_sw\_XX\_YYYY gde se umesto XX\_YYYY navodi broj indeksa - broj upisa i godina upisa (primer: projekat1\_sw\_02\_2019)
*   Ubaciti fajl u **zip** arhivu i nazvati je isto kao i zadatak (projekat1\_sw\_XX\_YYYY.zip)
*   Uploadovati zip arhivu kao assignment na enastavu.
*   Ukoliko bude problema sa uploadom, možete u predviđenom roku poslati zip na email adresu predmetnog asistenta.

Rok za slanje arhive je subota 30.05.2020.