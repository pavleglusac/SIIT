def fine(speed, limit):
    difference = speed - limit
    if difference > 0:
        fine = 5000 + 500*difference
        if speed > 120:
            fine += 10000
        return "Kazna iznosi " + str(fine) + " dinara."

    return "Niste prekoračili brzinu."



if __name__ == '__main__':
    print(fine(80,60))