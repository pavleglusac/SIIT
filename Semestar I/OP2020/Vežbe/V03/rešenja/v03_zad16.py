if __name__ == '__main__':
    n = int(input("Unesite za koliko prvih prirodnih brojeva želite da izračunate sumu kvadrata:"))
    sum = 0

    for number in range(1, n+1):
        sum += number**2

    print("Suma kvadrata prvih", n, "prirodnih brojeva iznosi", sum)
