
x:
		WORD	1
main:
		PUSH	%14
		MOV 	%15,%14
		SUBS	%15,$8,%15
@main_body:
		MOV 	$1,-4(%14)
		MOV 	$1,-8(%14)
@while_0:
		CMPS 	-4(%14),$3
		JGES	@end_while_0
		ADDS	-4(%14),$3,%0
		MOV 	%0,-4(%14)
		JMP	@while_0
@end_while_0:
@while_1:
		CMPS 	-8(%14),$3
		JGES	@end_while_1
		ADDS	-8(%14),-4(%14),%0
		MOV 	%0,-8(%14)
		JMP	@while_1
@end_while_1:
		MOV 	-8(%14),%13
		JMP 	@main_exit
@main_exit:
		MOV 	%14,%15
		POP 	%14
		RET