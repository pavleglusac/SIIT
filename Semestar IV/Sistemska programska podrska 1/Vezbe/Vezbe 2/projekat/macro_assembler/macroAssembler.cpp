#include "macroAssembler.h"

#include<fstream>
#include<iostream>

using namespace std;

bool macroAssembler(string input_file, string output_file)
{
	string			sourceLine;
	long			lineNumber = 0;

	bool			macroDefOn = false;
	DIRECTIVE_T		directive;
	OPTYPE_T		operationType = NONE;
	string			macroName;

	PARAM_STACK		fictiveParams;
	PARAM_STACK		realParams;
	string			macroDefinitionLine;
	string			macroDefinitionRealParamLine;
	const OPCODE*	instructionOpCode = NULL;
	string			cmd;
	string			executable;

	TMD_MAP			tmd;					// macro definitions table
	short			tmdIndex = 0;			// macro definition table index

	/******************************************************************************************
	* Open input and output files
	*******************************************************************************************/

	ifstream inFile(input_file);
	if (!inFile)
	{
		inFileOpenError(input_file);
		return false;
	}

	ofstream outFile(output_file);
	if (!outFile)
	{
		outFileOpenError(output_file);
		return false;
	}

	/******************************************************************************************
	* Read the contents of the input file
	*******************************************************************************************/
	while (!inFile.eof())
	{
		getline(inFile, sourceLine);
		lineNumber++;

		// ----------------------------------------------------------------------------------------
		// MACRO PROCESOR
		// sourceLine; lineNumber

		directive = getDirective(sourceLine, lineNumber);

		switch (directive)
		{
		case D_MACRO:
			macroDefOn = true;
			// get macro start directive
			getMacroNameAndParams(macroName, fictiveParams, sourceLine, lineNumber);
			// add entry to the opcode table
			pushInstr(macroName, MACRO, I_NONE, (tmdIndex >> 8) & 0xff, tmdIndex & 0xff, (char)fictiveParams.size());
			// add macro instruction to macro instruction list
			tmd[tmdIndex].fictiveParams = fictiveParams;
			tmd[tmdIndex].definition.push_back(sourceLine);
			break;
		case D_END_MACRO:
			// macro end (termination) directive
			tmd[tmdIndex].definition.push_back(sourceLine);
			// terminate current macro definition
			macroDefOn = false;
			tmdIndex++;
			break;
		default:
			if (macroDefOn)
			{
				// add source line of the current macro definition to TMD
				tmd[tmdIndex].definition.push_back(sourceLine);
			}
			else
			{
				// ----------------------------------------------------------------------------------------
				// if macro instruction replace it with macro definition with real parameters
				if (getExecutable(executable, sourceLine, lineNumber, false))
				{
					getCommand(cmd, sourceLine);
					
					if (getInstr(cmd, instructionOpCode) == 2)
					{
						outFile << "#" << sourceLine << endl;	
						getCommand(cmd, sourceLine);
						getParams(realParams, sourceLine, lineNumber);
						short ind = instructionOpCode->code << 8;
						ind |= instructionOpCode->funct;
						std::list<std::string> def_list = tmd[ind].definition;
						std::list<std::string>::iterator def_list_it = def_list.begin();
						def_list_it++;
						for (; def_list_it != def_list.end(); def_list_it++)
						{
							fictiveToRealParams(*def_list_it, macroDefinitionRealParamLine, tmd, ind, realParams);
							outFile << macroDefinitionRealParamLine << endl;
							if (next(next(def_list_it)) == def_list.end())
								break;
						}

					}
					else
					{
						outFile << sourceLine << endl;
					}
				}
				else {
					outFile << sourceLine << endl;
				}
				// copy original line otherwise
			}
			break;
		}
	}

	inFile.close();
	outFile.close();

	return !errorsFound();
}
