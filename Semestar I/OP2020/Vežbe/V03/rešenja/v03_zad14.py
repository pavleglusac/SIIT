from math import radians, sin

if __name__ == '__main__':
    height = eval(input("Unesite visinu:"))
    angle_degrees = eval(input("Unesite ugao u stepenima:"))
    angle_radians = radians(angle_degrees)
    ladder_height = height/sin(angle_radians)
    print("Visina merdevina bi trebalo da bude", ladder_height)
