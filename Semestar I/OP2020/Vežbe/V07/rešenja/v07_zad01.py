def count(V, T):
    return 3.74 + 0.6215*T - 35.75*(V**0.16)+0.4275*T*(V**0.16)

def print_table(v0, v1, t0, t1):
    print("{:10}".format(""), end="")
    for t in range(t0, t1 + 1):
        string_to_print = "t={}".format(t)
        print("{:<10}".format(string_to_print), end="")

    print()

    for v in range(v0, v1+1):
        string_to_print = "v={}".format(v)
        print("{:10}".format(string_to_print), end="")
        for t in range(t0, t1+1):
            print("{:<10.3f}".format(count(v, t)), end="")

        print()

if __name__ == '__main__':
    print_table(0, 10, 5, 10)

