package Zadatak3;
import java.util.Scanner;
public class Zadatak3 
{

	public static void main(String[] args)
	{
		var sc = new Scanner(System.in);
		int godina;
		System.out.println("Unesite neku godinu izmedju 1538 i 10000:");
		godina = sc.nextInt();
		boolean prestupna = false;
		if(godina % 400 == 0)
			prestupna = true;
		else
		{
			if(godina % 100 == 0)
				prestupna = false;
			else
				if(godina % 4 == 0)
				    prestupna = true;
				else
					prestupna = false;
		}
		if(prestupna)
		{
			System.out.println("Ova godina je prestupna!");
		}
		else
		{
			System.out.println("Ova godina nije prestupna!");
		}
		sc.close();
	}

}
