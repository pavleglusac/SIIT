library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity aut is port (
    iCLK : in std_logic;
    inRST : in std_logic;
    oRED : out std_logic;
    oGREEN : out std_logic;
    oBLUE : out std_logic
);
end aut;

architecture behavioral of aut is

    signal sD : std_logic_vector(3 downto 0);
    signal sWR_EN : std_logic;
    signal sCNT : std_logic_vector(3 downto 0);

    type tSTATE is (IDLE, RED, WRITE_CNT, GREEN, BLUE);
    signal sSTATE, sNEXT : tSTATE;
    
begin

    -- automat: registar
    process (iCLK, inRST) begin
        if (inRST = '0') then
            sSTATE <= IDLE;
        elsif (iCLK'event and iCLK = '1') then
            sSTATE <= sNEXT;
        end if;
    end process;

    -- automat: funkcija prelaza
    process (sSTATE, sCNT) begin
        case (sSTATE) is
            when IDLE =>
                if (sCNT = "0011") then
                    sNEXT <= RED;
                else
                    sNEXT <= IDLE;
                end if;
            when RED =>
                if (sCNT = "0101") then
                    sNEXT <= GREEN;
                else
                    sNEXT <= RED;
                end if;
            when GREEN =>
                if (sCNT = "0111") then
                    sNEXT <= WRITE_CNT;
                else
                    sNEXT <= GREEN;
                end if;
            when WRITE_CNT =>
                sNEXT <= BLUE;
            when BLUE =>
                if (sCNT = "0000") then
                    sNEXT <= IDLE;
                else
                    sNEXT <= BLUE;
                end if;
            when others =>
                sNEXT <= IDLE;
        end case;
    end process;

    -- automat: funkcija izlaza
    process (sSTATE) begin
        case (sSTATE) is
            when WRITE_CNT =>
                sD <= "0110";
                sWR_EN <= '1';
            when others =>
                sD <= "0000";
                sWR_EN <= '0';
        end case;
    end process;

    -- brojac
    process (iCLK, inRST) begin
        if (inRST = '0') then
            sCNT <= "0000";
        elsif (iCLK'event and iCLK = '1') then
            if (sWR_EN = '1') then
                sCNT <= sD;
            elsif (sCNT = 8) then
                sCNT <= "0000";
            else
                sCNT <= sCNT + 1;
            end if;
        end if;
    end process;

    -- k.m.
    oRED <= '1' when sSTATE = RED else '0';
    oGREEN <= '1' when sSTATE = GREEN else '0';
    oBLUE <= '1' when sSTATE = BLUE else '0';

end behavioral;
