
main:
		PUSH	%14
		MOV 	%15,%14
		SUBS	%15,$12,%15
@main_body:
		MOV 	$22,-4(%14)
		DIVS	-4(%14),$2,%0
		MOV 	%0,-4(%14)
		MOV 	$5,-8(%14)
		MULS	$5,$2,%0
		MOV 	%0,-8(%14)
		MOV 	$100,-12(%14)
		ADDS	-4(%14),-8(%14),%0
		MULS	$2,-12(%14),%1
		ADDS	%0,%1,%0
		DIVS	-12(%14),$4,%1
		ADDS	%0,%1,%0
		MOV 	%0,-12(%14)
		MOV 	-12(%14),%13
		JMP 	@main_exit
@main_exit:
		MOV 	%14,%15
		POP 	%14
		RET