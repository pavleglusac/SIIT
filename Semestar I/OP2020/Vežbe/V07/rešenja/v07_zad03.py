def next_member(x):
    if x % 2 == 0:
        return x//2
    else:
        return 3*x+1


def syracuse(x):
    next_number = next_member(x)
    result = [x]
    while next_number > 1:
        result.append(next_number)
        next_number = next_member(next_number)

    result.append(1)
    return result


if __name__ == '__main__':
    print(syracuse(9))

