def bmi(mass, height):
    bmi = mass/(height**2)

    if bmi < 18.5:
        return "pothranjenost"

    if bmi < 25:
        return "idealna telesna masa"

    if bmi < 30:
        return "preterana telesna masa"

    return "gojaznost"

if __name__ == '__main__':
    print(bmi(77, 1.8))
