package automobili;

public class Tocak {
	double pritisak;
	
	Tocak() {
		pritisak = 1;
	}
	
	Tocak(Tocak b) {
		this.pritisak = b.pritisak;
	}

	Tocak copy() {
		Tocak ret = new Tocak();
		ret.pritisak = this.pritisak;
		return ret;
	}
}
