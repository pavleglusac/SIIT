def characteristics(array):
    print("Najmanji element niza", array, "je", minimum(array))
    print("Najveći element niza", array, "je", maximum(array))
    print("Suma elemenata niza", array, "je", sum(array))
    print("Prosek elemenata niza", array, "je", average(array))

def minimum(array):
    minimal = array[0]
    for element in array:
        if element<minimal:
            minimal = element

    return minimal

def maximum(array):
    maximal = array[0]
    for element in array:
        if element > maximal:
            maximal = element

    return maximal

def sum(array):
    sum = 0
    for element in array:
        sum += element

    return sum

def average(array):
    return sum(array)/len(array)

if __name__ == '__main__':
    characteristics([1,2,3,4,5,-1,6])