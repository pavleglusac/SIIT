def eliminate_duplicates(words):
    # Evidentiramo prvo pojavljivanje svake reči i čuvamo u listi bez duplikata unique_elements
    unique_elements = []
    for word in words:

        # Za svaku reč na koju naiđemo proveravamo da li se već nalazi u listi unikata
        # Uvodimo indikator unique da bismo sačuvali podatak o jedinstvenosti trenutne reči
        unique = True
        for unique_element in unique_elements:
            # Ako nađemo istu reč trenutnoj, zaključujemo da je trenutna reč duplikat
            if word == unique_element:
                unique = False
                # Možemo da prekinemo poređenja za trenutnu reč, imamo dovoljno informacija da znamo da ne treba da je
                # dodajemo u listu unikata
                break

        # Ukoliko smo utvrdili da reč nije duplikat, dodajemo je u listu unikata
        if unique:
            unique_elements.append(word)

    return " ".join(unique_elements)


if __name__ == '__main__':
    print(eliminate_duplicates(['hello', 'world', 'hello', 'and', 'practice', 'and', 'makes', 'perfect', 'and', 'hello', 'world', 'again']))