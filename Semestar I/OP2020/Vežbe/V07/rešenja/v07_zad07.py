usernames = []
passwords = []

item_names = []
item_prices = []
item_amounts = []

def load_user_data(users_path):

    users_file = open(users_path)

    for line in users_file.readlines():
        user_data = line.replace("\n", "").split("|")
        usernames.append(user_data[0])
        passwords.append(user_data[1])

    users_file.close()


def load_items_data(items_path):
    items_file = open(items_path)

    for line in items_file.readlines():
        item_data = line.replace("\n", "").split("|")
        item_names.append(item_data[0])
        item_prices.append(float(item_data[1]))
        item_amounts.append(float(item_data[2]))

    items_file.close()


def check_username_and_password(username, password):

    for index in range(len(passwords)):
        if passwords[index] == password and usernames[index] == username:
            return True

    return False

def login():
    username = input("Unesite korisničko ime:")
    password = input("Unesite lozinku:")

    correct = check_username_and_password(username, password)

    if correct:
        print("Uspešno ste se ulogovali.")
    else:
        print("Pogrešno korisničko ime i/ili lozinka.")

    return correct


def register():

    item_name = input("Unesite naziv proizvoda:")
    item_price = float(input("Unesite cenu proizvoda:"))
    item_amount = float(input("Unesite količinu proizvoda:"))

    item_names.append(item_name)
    item_prices.append(item_price)
    item_amounts.append(item_amount)

    print_items()

def save_items_to_file(file_path):
    items_file = open(file_path, "w")

    for index in range(len(item_names)):
        items_file.write(item_names[index] + "|" + str(item_prices[index]) + "|" + str(item_amounts[index]) + "\n")

    items_file.close()

def print_items():

    print("{:20}{:10}{:10}".format("Naziv", "Cena", "Količina"))
    print("="*45)
    for i in range(len(item_names)):
        print("{:20}{:10.2f}{:10}".format(item_names[i], item_prices[i], item_amounts[i]))

def print_menu():
    while True:
        print("="*10, "MENU", "="*10)
        print("1. Dodavanje novog proizvoda.")
        print("2. Ispis svih proizvoda.")
        users_input = input("Unesite jedan od ponuđenih brojeva za odabir opcije, 'quit' za kraj:")

        if users_input == "quit":
            break

        if users_input == '1':
            register()

        elif users_input == '2':
            print_items()

        else:
            print("Odabrali ste nepostojeću opciju.")

def main():
    load_items_data("../fajlovi/proizvodi.txt")
    load_user_data("../fajlovi/prodavci.txt")
    correct = login()
    while not correct:
        correct = login()

    print_menu()
    save_items_to_file("../fajlovi/proizvodi.txt")



if __name__ == '__main__':
    main()


