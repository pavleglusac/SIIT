package izuzeci;

/**
 * Klasa koja reprezentuje jednu osobu (�alim se).
 * @author minja
 *
 */
public class Test {
	/**
	 * Ra�una prose�nu visinu.
	 * @param i to je visina
	 * @throws MojException
	 */
	public void f(int i) throws MojException {
		if (Math.random() > 0.5) {
			throw new MojException("Greska");
		}
	}
	public static void main(String[] args) {
		Test t = new Test();
		try {
			t.f(4);
		} catch (MojException e) {
			e.printStackTrace();
		}
	}

}
