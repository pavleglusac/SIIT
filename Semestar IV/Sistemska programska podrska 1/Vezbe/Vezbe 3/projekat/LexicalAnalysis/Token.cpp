#include <iostream>
#include <iomanip>

#include "Token.h"
#include "FiniteStateMachine.h"

using namespace std;


Token::Token() : m_tokenType(START), m_value("")
{}

TokenType Token::getType()
{
	return m_tokenType;
}


void Token::setType(TokenType t)
{
	m_tokenType = t;
}


string Token::getValue()
{
	return m_value;
}


void Token::setValue(string s)
{
	m_value = s;
}


string Token::tokenTypeToString()
{
	switch (m_tokenType)
	{
		case START:				return "[START]";
		case NO_FINITE:			return "[NO_FINITE]";
		case COMMENT:			return "[COMMENT]";
		case ID:				return "[ID]";
		case NUM:				return "[NUM]";
		case REAL:				return "[REAL]";
		case IF:				return "[IF]";
		case THEN:				return "[THEN]";
		case ELSE:				return "[ELSE]";
		case PLUS:				return "[PLUS]";
		case MINUS:				return "[MINUS]";
		case EQ:				return "[EQ]";
		case SEMI:				return "[SEMI]";
		case WHITE_SPACE:		return "[WHITE_SPACE]";
		case ERROR:				return "[ERROR]";
		case END_OF_FILE:		return "[EOF]";
		default:				return "";
	}
}
