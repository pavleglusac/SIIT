#include "Counter.h"
#include <iostream>
#include "Globals.h"

struct Tally {
	HashMapa& table;
	Tally(HashMapa& table_) : table(table_) {}
	void operator()(const blocked_range<unsigned int> range) const {
		HashMapa::accessor a;
		for (unsigned int p = range.begin(); p != range.end(); ++p) {
			table.insert(a, clip2CounterVector[p]);
			a->second += 1;
			a.release(); //the lock on object
		}
	}
};

RetVal Counter()
{
	int n = clip2CounterVector.size();

	parallel_for(blocked_range<unsigned int>(0, n), Tally(mapa));

    return RET_OK;
}
