library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity t1 is port (
    iCLK : in std_logic;
    inRST : in std_logic;
    iD : in std_logic;
    iEN : in std_logic;
    iSEL : in std_logic_vector(1 downto 0);
    oQ : out std_logic_vector(4 downto 0)
);
end t1;

architecture Behavioral of t1 is

    signal sDATA : std_logic_vector(9 downto 0);
    signal sLIM : std_logic_vector(4 downto 0);
    signal sASHR : std_logic_vector(4 downto 0);
    signal sMUX : std_logic_vector(4 downto 0);
    signal sREG : std_logic_vector(4 downto 0);
    signal sCNT : std_logic_vector(3 downto 0);
    signal sTC : std_logic;

begin

    -- Izlaz
    oQ <= sREG;

    -- Shift_reg
    process (iCLK, inRST) begin
        if (inRST = '0') then
            sDATA <= (others => '0');
        elsif (iCLK'event and iCLK = '1') then
            -- funkcija promene vrednosti
            if (iEN = '1') then
                sDATA <= iD & sDATA(9 downto 1);
            end if;
        end if;
    end process;

    -- Cnt
    process (iCLK, inRST) begin
        if (inRST = '0') then
            sCNT <= (others => '0');
        elsif (iCLK'event and iCLK = '1') then
            -- funkcija promene vrednosti
            if (iEN = '1') then
                if (sCNT = 9) then
                    sCNT <= (others => '0');
                else
                    sCNT <= sCNT + 1;
                end if;
            end if;
        end if;
    end process;

    -- Komparator unutar brojaca
    sTC <= '1' when sCNT = 0 else '0';

    -- Limiter
    sLIM <= "11000" when sDATA(4 downto 0) > 24 else
            sDATA(4 downto 0);

    -- Ashr2
    sASHR <= sDATA(9) & sDATA(9) & sDATA(9 downto 7);

    -- Mux
    sMUX <= sLIM when iSEL = "00" else
            sDATA(4 downto 0) when iSEL = "01" else
            sDATA(9 downto 5) when iSEL = "10" else
            sASHR;

    -- Reg
    process (iCLK, inRST) begin
        if (inRST = '0') then
            sREG <= (others => '0');
        elsif (iCLK'event and iCLK = '1') then
            -- funkcija promene vrednosti
            if (sTC = '1') then
                sREG <= sMUX;
            end if;
        end if;
    end process;

end Behavioral;
