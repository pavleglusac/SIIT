#include "LexicalAnalysis.h"

#include <iostream>
#include <exception>

using namespace std;

void main()
{
	try
	{
		bool retVal = false;

		LexicalAnalysis lex;
		if (!lex.readInputFile(".\\..\\program.txt"))
			throw runtime_error("\nException! Failed to open input file!\n");
		
		lex.initialize();

		if (lex.Do())
		{
			cout << "Lexical analysis finished successfully!" << endl;
			lex.printTokens();
		}
		else
		{
			cout << "Lexical analysis failed!" << endl;
			lex.printLexError();
		}
	}
	catch (runtime_error e)
	{
		cout << e.what() << endl;
	}
}