package Zadatak13;

import java.util.Scanner;

public class Zadaatak13 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Uneti n: ");
		int n = sc.nextInt();
		System.out.println("Uneti m: ");
		int m = sc.nextInt();
		int suma = 0;
		for(int i = 1; i <= n; i++)
		{
			for(int j = 1; j <= m; j++)
			{
				System.out.printf("%6d", i * i);
				if(i<j)
					suma += i*i;
			}
			System.out.println();
		}
		System.out.println("Suma elemenata iznad glavne dijagonale: " + suma);
	}
}
