class EmptyListException(Exception):
    pass

class Node(object):

    def __init__(self, data, next=None, prev=None):
        self.data = data
        self.next = next
        self.prev = prev

    def __str__(self):
        return str(self.data)

class DoublyLinkedList(object):

    def __init__(self):
        self._head = Node(None)
        self._tail = Node(None)

        self._head.next = self._tail
        self._tail.prev = self._head

    def add_first(self, data):
        node = Node(data)

        node.next = self._head.next
        self._head.next.prev = node
        self._head.next = node
        node.prev = self._head

    def remove_first(self):
        if self._head.next is self._tail:
            raise EmptyListException("List is empty.")

        else:
            current = self._head.next
            current.next.prev = self._head
            self._head.next = current.next


    def __iter__(self):

        if self._head.next is not self._tail:
            current = self._head.next

            while current is not self._tail:
                yield current
                current = current.next


if __name__ == '__main__':
    lista = DoublyLinkedList()
    lista.add_first(1)
    lista.add_first(2)
    lista.add_first(3)

    lista.remove_first()

    for item in lista:
        print(item)
