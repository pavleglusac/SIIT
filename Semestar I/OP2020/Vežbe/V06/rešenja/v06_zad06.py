def easter(year):
    if 1982 <= year <= 2048:
        a = year % 19
        b = year % 4
        c = year % 7
        d = (19*a + 24) % 30
        e = (2*b + 4*c + 6*d + 5) % 7

        date = 22 + d + e
        if date <= 31:
            month = "Mart"
        else:
            date -= 31
            month = "April"

        return date, month

    #ako godina nije u predviđenom opsegu vraća se -1, -1
    return -1, -1


if __name__ == '__main__':
    year = int(input("Unesite godinu:"))

    date, month = easter(year)
    
    if date != -1 and month != -1:
        print("Datum Uskrsa po gregorijanskom kalendaru je " + str(date) + ". " + month + " " + str(year) + ".")
    else:
        print("Godina nije u predviđenom opsegu.")
