from v05_zad03 import reading

def writing(username, password, file_path, delimiter):
    file = open(file_path, "a")

    file.write(username+delimiter+password+"\n")

    file.close()

    return reading(file_path, delimiter)

if __name__ == '__main__':
    print("\n\nZadatak 4:\n")
    print(writing("jovica", "jovic", "../fajlovi/korisnici.txt", "|"))