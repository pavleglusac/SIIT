
----------------------------------------------------------------------------------
-- Logicko projektovanje racunarskih sistema 1
-- 2011/2012,2020
--
-- Computer system top level
--
-- author:
-- Ivan Kastelan (ivan.kastelan@rt-rk.com)
-- Milos Subotic (milos.subotic@uns.ac.rs)
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity top is
	port(
		iCLK       : in  std_logic;
		inRST      : in  std_logic;
		oSUBSEQS   : out std_logic_vector(15 downto 0);
		oDONE      : out std_logic
	);
end entity top;

architecture Behavioral of top is

	component cpu_top is
		port(
			iCLK : in  STD_LOGIC;
			inRST : in  STD_LOGIC;
			iINSTR : in  STD_LOGIC_VECTOR (14 downto 0);
			iDATA : in  STD_LOGIC_VECTOR (15 downto 0);
			oPC : out  STD_LOGIC_VECTOR (15 downto 0);
			oDATA : out  STD_LOGIC_VECTOR (15 downto 0);
			oADDR : out  STD_LOGIC_VECTOR (15 downto 0);
			oMEM_WE : out  STD_LOGIC;
			oLED : out  STD_LOGIC_VECTOR (15 downto 0)
		);
	end component;

	component instr_rom is
		port(
			iA : in  std_logic_vector(15 downto 0);
			oQ : out std_logic_vector(14 downto 0)
		);
	end component instr_rom;

	component data_ram is
		port(
			iCLK  : in  std_logic;
			inRST : in  std_logic;
			iA    : in  std_logic_vector(7 downto 0);
			iD    : in  std_logic_vector(15 downto 0);
			iWE   : in  std_logic;
			oQ    : out std_logic_vector(15 downto 0)
		);
	end component data_ram;
	
	component reg is
		generic(
			WIDTH : integer := 16
		);
		port(
			iCLK  : in  std_logic;
			inRST : in  std_logic;
			iD    : in  std_logic_vector(WIDTH-1 downto 0);
			iWE   : in  std_logic;
			oQ    : out std_logic_vector(WIDTH-1 downto 0)
		);
	end component reg;
	
	--TODO SEQUENCE ROM component
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	-- pravljenje komponente koja se koristi
	
	component sequence_rom is
	port(
		iA : in  std_logic_vector(7 downto 0);
		oQ : out std_logic_vector(1 downto 0)
	);
   end component sequence_rom;
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	signal sINSTR  : std_logic_vector(14 downto 0);
	signal sPC     : std_logic_vector(15 downto 0);
	signal sBUS_A  : std_logic_vector(15 downto 0);
	signal sBUS_RD : std_logic_vector(15 downto 0);
	signal sMEM_RD : std_logic_vector(15 downto 0);
	signal sSUBSEQS_RD : std_logic_vector(15 downto 0);
	signal sDONE_RD : std_logic_vector(15 downto 0);
	signal sSEQ_ROM_RD : std_logic_vector(15 downto 0);
	signal sBUS_WD : std_logic_vector(15 downto 0);
	signal sBUS_WE : std_logic;
	signal sMEM_WE : std_logic;
	signal sSUBSEQS_WE : std_logic;
	signal sDONE_WE : std_logic;
	

begin

	cpu_top_i: cpu_top
	port map(
		iCLK    => iCLK,
		inRST   => inRST,
		oPC     => sPC,
		iINSTR  => sINSTR,
		oADDR   => sBUS_A,
		oDATA   => sBUS_WD,
		oMEM_WE => sBUS_WE,
		iDATA   => sBUS_RD,
		oLED    => open
	);

	i_instr_rom: instr_rom
	port map(
		iA => sPC,
		oQ => sINSTR
	);

	i_data_ram: data_ram
	port map(
		iCLK  => iCLK,
		inRST => inRST,
		iA    => sBUS_A(7 downto 0),
		oQ    => sMEM_RD,
		iD    => sBUS_WD,
		iWE   => sMEM_WE
	);
	
	--TODO SEQUENCE ROM instance
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	-- Pravljenje SEQUENCE ROM 
	
	i_sequence_rom : sequence_rom 
	port map(
	
		iA    => sBUS_A(7 downto 0),
		oQ    => sSEQ_ROM_RD(1 downto 0)
	);
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	--TODO SUBSEQS register instance
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	--Pravljenje SUBSEQS registra
	
	i_subseqs_req : reg
		port map(
		iCLK  => iCLK,
		inRST => inRST,
		iD    => sBUS_WD,
		iWE   => sSUBSEQS_WE,
		oQ    => sSUBSEQS_RD
	);
	oSUBSEQS <= sSUBSEQS_RD;
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	-- DONE register instance
	i_done_reg: reg
	port map(
		iCLK  => iCLK,
		inRST => inRST,
		iD    => sBUS_WD,
		iWE   => sDONE_WE,
		oQ    => sDONE_RD
	);
	oDONE <= sDONE_RD(0);
	
	
	-- Data Write Enable demux.
	process(sBUS_A, sBUS_WE)
	begin
		sMEM_WE <= '0';
		--TODO SUBSEQS WE default
		
		--------------------------------------------------------------------------------------------------------------------------------
		
		-- deafault je 0 za dozvolu upisa
		
		sSUBSEQS_WE <= '0';		
	
		--------------------------------------------------------------------------------------------------------------------------------
		
		
		sDONE_WE <= '0';
		case sBUS_A(11 downto 8) is
			when x"0" =>
				sMEM_WE <= sBUS_WE;
			when x"1" =>
				--TODO SUBSEQS WE
				
		--------------------------------------------------------------------------------------------------------------------------------
		
		-- u zadatku pise koristiti 0x0170, zato koristimo 70 dole i tad imamo dozvolu za upis 
				
				if sBUS_A(7 downto 0) = x"70" then 
				sSUBSEQS_WE <= sBUS_WE;
				end if;
		--------------------------------------------------------------------------------------------------------------------------------
		
			when x"2" =>
				if sBUS_A(7 downto 0) = x"00" then
					-- Write on address 0x0200.
					sDONE_WE <= sBUS_WE;
				end if;
			when others =>
		end case;
	end process;
	-- Data Read mux.
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	-- dodajemo u listu osetljivosti  sSEQ_ROM_RD i sSUBSEQS_RD
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	process(sBUS_A, sMEM_RD, sDONE_RD, sSEQ_ROM_RD, sSUBSEQS_RD)
	begin
		sBUS_RD <= x"baba";
		case sBUS_A(15 downto 8) is
			when x"00" =>
				sBUS_RD <= sMEM_RD;
			when x"01" =>
			
				--TODO SUBSEQS read data
				
	--------------------------------------------------------------------------------------------------------------------------------
	
	-- u zadatku pise koristiti 0x0170, zato koristimo 70 dole i tad imamo dozvolu za citanje 
	
				if sBUS_A(7 downto 0) = x"70" then 
						sBUS_RD <= sSUBSEQS_RD;
				end if;
				
	--------------------------------------------------------------------------------------------------------------------------------
			when x"02" =>
				-- DONE read data
				if sBUS_A(7 downto 0) = x"00" then
					-- Read on address 0x0200.
					sBUS_RD <= sDONE_RD;
				end if;
			--TODO SEQUENCE ROM read data
			
	--------------------------------------------------------------------------------------------------------------------------------
	
	-- u zadatku pise koristiti 0x1600, zato koristimo 16 dole i tad imamo dozvolu za upis
	--	stavljamo na nulu sve sto nisu najmanje znacjanja dva bita
	
			when x"16" => 
				sBUS_RD <= x"000"&"00" & sSEQ_ROM_RD(1 downto 0);
	
	--------------------------------------------------------------------------------------------------------------------------------
			when others =>
		end case;
	end process;

end architecture;
