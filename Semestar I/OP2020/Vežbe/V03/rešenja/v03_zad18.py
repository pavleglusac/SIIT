if __name__ == '__main__':
    n = int(input("Unesite koliko brojeva želite da upotrebite za izračunavanje proseka:"))
    sum = 0

    for number in range(n):
        sum += eval(input(str(number+1)+". Unesite broj:"))

    print("Prosek unetih brojeva iznosi", round(sum/n, 2))
