from math import sqrt, ceil

def is_prime(x):
    if x == 1:
        return False
    max_divisor = ceil(sqrt(x))
    divisor = 2
    while divisor <= max_divisor:
        if x % divisor == 0:
            return False
        divisor += 1

    return True

if __name__ == '__main__':
    print(is_prime(127))


