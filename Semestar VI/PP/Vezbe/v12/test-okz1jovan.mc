//OPIS: branch
//RETURN: 1007
int main(){
  int a;
  int y;
  a = 1;
  y = 0;
  
  branch(a ; 1 , 3 , 5)
  	first a = a + a;
  	second a = a - a;
  	third a = a + 0;
  	otherwise a = a + 1000;
  	
  y = a + y;
  
  a=3;
  
    branch(a ; 1 , 3 , 5)
  	first a = a + a;
  	second a = a - a;
  	third a = a + 0;
  	otherwise a = a + 1000;
  	
  y = a + y;
  
    a=5;
  
    branch(a ; 1 , 3 , 5)
  	first a = a + a;
  	second a = a - a;
  	third a = a + 0;
  	otherwise a = a + 1000;
  	
  y = a + y;
  
      a=0;
  
    branch(a ; 1 , 3 , 5)
  	first a = a + a;
  	second a = a - a;
  	third a = a + 0;
  	otherwise a = a + 1000;
  	
  y = a + y;
  

  
  return y;
}

