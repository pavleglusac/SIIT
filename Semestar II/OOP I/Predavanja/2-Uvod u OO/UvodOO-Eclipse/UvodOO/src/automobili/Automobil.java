package automobili;

public class Automobil {

	public static final int BROJ_VRATA = 4;
	
	static int i;
	
	private boolean radi;
	
	private Tocak prednjiLevi;
	private Tocak prednjiDesni;

	public Automobil() {
		prednjiDesni = new Tocak();
		prednjiLevi = new Tocak();
	}
	
	Automobil(Automobil b) {
		this.prednjiDesni = new Tocak(b.prednjiDesni);
		this.prednjiLevi  = new Tocak(b.prednjiLevi);
	}
	
	void upali() {
		radi = true;
		System.out.println("upalio automobil");
		System.out.println(BROJ_VRATA);
	}

	void ugasi() {
		radi = false;
	}

	Automobil copy() {
		Automobil ret = new Automobil();
		ret.prednjiDesni = this.prednjiDesni.copy();
		ret.prednjiLevi  = this.prednjiLevi.copy();
		return ret;
	}
	
	static void f() {
		System.out.println("f");
		i = 18;
	}
	
	public Tocak getPrednjiDesni() {
		return prednjiDesni;
	}
	
	public void setPrednjiDesni(Tocak t) {
		prednjiDesni = t;
	}
	
	public static void main(String[] args) {
		System.out.println("main u automobilu");
	}
}
