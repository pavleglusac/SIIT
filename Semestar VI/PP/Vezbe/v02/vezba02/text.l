%option noyywrap yylineno
%{
  #include "text.tab.h"
%}

%%

[ \t]+    { /* skip */ }

"."         { return DOT; }
"!"         { return UZVIK; }
"?"         { return UPIT; }
","         { return ZAREZ; }


[A-Z][a-z]* { return CAPITAL_WORD; }
[a-z]+      { return WORD; }
\n+         { return NL;}
                     
.           { printf("\nline %d: LEXICAL ERROR on char %c", 
                      yylineno, *yytext); }    

