package Zadatak10;
import java.util.Scanner;

public class Zadatak10 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Uneti n: ");
		int n = sc.nextInt();
		int A[][] = new int[n][n];
		for(int i = 0; i < n; i++)
			for(int j = 0; j < n; j++)
			{
				System.out.printf("A[%d][%d] = ", i, j);
				A[i][j] = sc.nextInt();
			}
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
				System.out.printf("%4d", A[i][j]);
			System.out.println();
		}
		int pom;
		for(int i = 0; i < n; i++)
			for(int j = 0; j < n; j++)
				if(i == j)
				{
					pom = A[i][j];
					A[i][j] = A[i][n-j-1];
					A[i][n-j-1] = pom;
				}
		System.out.println("\n");
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
				System.out.printf("%4d", A[i][j]);
			System.out.println();
		}
				
	}
}
