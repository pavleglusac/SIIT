
library ieee;
use ieee.std_logic_1164.all;

library work;

entity lprs1_homework2_tb is
end entity;

architecture arch of lprs1_homework2_tb is
	
	constant i_clk_period : time := 25 ns; -- 40 MHz
	
	signal i_clk    : std_logic;
	signal i_rst    : std_logic;
	signal i_run    : std_logic;
	signal i_pause  : std_logic;
	
	signal o_digit0 : std_logic_vector(3 downto 0);
	signal o_digit1 : std_logic_vector(3 downto 0);
	signal o_digit2 : std_logic_vector(3 downto 0);
	signal o_digit3 : std_logic_vector(3 downto 0);
	
begin
	
	uut: entity work.lprs1_homework2
	port map(
		i_clk    => i_clk,
		i_rst    => i_rst,
		i_run    => i_run,
		i_pause  => i_pause,
		o_digit0 => o_digit0,
		o_digit1 => o_digit1,
		o_digit2 => o_digit2,
		o_digit3 => o_digit3
	);
	
	clk_p: process
	begin
		i_clk <= '1';
		wait for i_clk_period/2;
		i_clk <= '0';
		wait for i_clk_period/2;
	end process;
	
	stim_p: process
	begin
		-- Test cases:
		
		-- JOVAN SRDANOV RA 145/2019
		
		-----------------------   1   -----------------------
		
		i_rst <='0';
		
		i_run <='0';
		
		i_pause <='0';
		
		i_rst <= '1';

		wait for 1000ns-i_clk_period;	--975ns
		
		i_rst <= '0'; 
		
		wait for i_clk_period; 			--1000ns
		
		----------------------- 2 i 3 -----------------------
		
		i_run <= '1';							
		
		wait for i_clk_period; 			--1025ns
		
		i_run <= '0';
		
		-----------------------   4   -----------------------   

		wait for 1975ns;  				--3000ns
		
		i_pause <= '1';						
		
		wait for i_clk_period; 			--3025ns	
		
		i_pause <= '0';
		
		i_run <= '1';							
		
		wait for i_clk_period; 			--3050ns	
		
		i_run <= '0';	
		
		-----------------------   5   -----------------------
		
		i_run <= '1';
		
		-----------------------   6   -----------------------
		
		wait for 1975ns;					--5025ns
		
		i_rst <= '1';	
		
		wait for 6000ns;					--11025ns
		
		i_rst <= '0';	
		
		-----------------------   7   -----------------------
		
		wait for 23025ns;					--34050ns
		
		i_rst <= '1';
		
		wait for i_clk_period;			--34075ns
		
		i_rst <= '0';	
		
		-----------------------   8   -----------------------
		
		wait for 25025ns;					--59100ns
		
		i_rst <= '1';
		
		wait for i_clk_period;			--59125ns
		
		i_rst <= '0';	
		
		wait;
	end process;
	
	
end architecture;
