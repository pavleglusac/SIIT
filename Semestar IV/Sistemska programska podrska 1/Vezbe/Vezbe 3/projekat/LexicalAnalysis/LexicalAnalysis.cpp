#include <iostream>
#include <iomanip>
#include <fstream>

#include "LexicalAnalysis.h"
#include "FiniteStateMachine.h"

using namespace std;

namespace {
	FiniteStateMachine g_fsm;
}

#define LEFT_ALIGN				20
#define RIGHT_ALIGN				25


void printTokenInfo(Token t)
{
	cout << setw(LEFT_ALIGN) << left << t.tokenTypeToString();
	cout << setw(RIGHT_ALIGN) << right << t.getValue() << endl;
}

/**
* Creates a token
* [in] begin - start position in the program buffer - first character of the token
* [in] end - end position in the program buffer - end character of the token
* [in] program - program buffer
* [in] lastFiniteState - number of the last finite state,
*		this is used to get the name of the state and store it as token type
*/
Token makeToken(int begin, int end, std::vector<char>& programBuffer, int lastFiniteState)
{
	string value = "";
	for (int i = begin; i < end; i++)
	{
		value += programBuffer[i];
	}
	return Token(FiniteStateMachine::getTokenType(lastFiniteState), value);
}


void LexicalAnalysis::initialize()
{
	m_programBufferPosition = 0;
	m_errorToken.setType(NO_TYPE);
	m_errorToken.setValue("");
	g_fsm.initStateMachine();
}


bool LexicalAnalysis::Do()
{
	while (true)
	{
		Token token = getNextTokenLex();
		switch (token.getType())
		{
			case ERROR:
				m_errorToken = token;
				m_tokenList.push_back(token);
				return false;
			case END_OF_FILE:
				m_tokenList.push_back(token);
				return true;
			case WHITE_SPACE:		// skip whitespaces
				continue;
			default:
				m_tokenList.push_back(token);
				break;
		}
	}
}


bool LexicalAnalysis::readInputFile(string fileName)
{
	m_inputFile.open(fileName, ios_base::binary);

	if (!m_inputFile)
		return false;
	
	m_inputFile.seekg(0, m_inputFile.end);
	int length = (int)m_inputFile.tellg();
	m_inputFile.seekg (0, m_inputFile.beg);
	m_programBuffer.resize(length);
	m_inputFile.read(&m_programBuffer.front(),length);
	m_inputFile.close();
	return true;
}


Token LexicalAnalysis::getNextTokenLex()
{
	int nextState = -1;
	int currentState = 1;
	// last finite state
	int lastFiniteState = 0;

	// position
	int counter = 0;
	int lastLetterPos = m_programBufferPosition;

	while(true)
	{
		char letter;
		unsigned int letterIndex = m_programBufferPosition + counter;
	
		if (letterIndex < m_programBuffer.size())
		{
			letter = m_programBuffer[letterIndex];
		}
		else
		{
			// we have reached the end of input file, force the search letter to invalid value
			letter = -1;
			if (m_programBufferPosition >= m_programBuffer.size())
			{
				// if we have reached end of file and printed out the last correct token
				// create EOF token and exit
				return Token(END_OF_FILE, "EOF");
			}
		}
		
		nextState = g_fsm.getNextState(currentState, letter);
		counter++;

		if (nextState > START_STATE)
		{
			if (nextState != NO_FINITE)
			{
				lastFiniteState = nextState;
				lastLetterPos = m_programBufferPosition + counter;
			}
			currentState = nextState;
		}
		else if (nextState == D_NOTHING)
		{
			if (lastFiniteState != START_STATE)
			{
				// make token
				Token token = makeToken(m_programBufferPosition, lastLetterPos, m_programBuffer, lastFiniteState);
				m_programBufferPosition = lastLetterPos;
				return token;
			}
			else
			{
				// error
				m_programBufferPosition = m_programBufferPosition + counter - 1;
				string s;
				s = m_programBuffer[m_programBufferPosition];
				return Token(ERROR, s);
			}
		}
		else
		{
			// end
			int len = lastLetterPos - m_programBufferPosition;

			if (len > 0)
			{
				// make token
				Token token = makeToken(m_programBufferPosition, lastLetterPos, m_programBuffer, lastFiniteState);
				m_programBufferPosition = lastLetterPos;
				return token;
			}
			else
			{
				// error
				m_programBufferPosition = m_programBufferPosition + counter - 1;
				string s;
				s = m_programBuffer[m_programBufferPosition];
				return Token(ERROR, s);
			}
		}
	}

	return Token();
}


TokenList& LexicalAnalysis::getTokenList()
{
	return m_tokenList;
}


void LexicalAnalysis::printTokens()
{
	if (m_tokenList.empty())
	{
		cout << "Token list is empty!" << endl;
	}
	else
	{
		printMessageHeader();
		TokenList::iterator it = m_tokenList.begin();
		for (; it != m_tokenList.end(); it++)
		{
			printTokenInfo(*it);
		}
	}
}


void LexicalAnalysis::printLexError()
{
	if (m_errorToken.getType() != NO_TYPE)
	{
		printMessageHeader();
		printTokenInfo(m_errorToken);
	}
	else
	{
		cout << "There are no lexical errors!" << endl;
	}
}


void LexicalAnalysis::printMessageHeader()
{
	cout << setw(LEFT_ALIGN) << left << "Type:";
	cout << setw(RIGHT_ALIGN) << right << "Value:" << endl;
	cout << setfill('-') << setw(LEFT_ALIGN+RIGHT_ALIGN+1) << " " << endl;
	cout << setfill(' ');
}
