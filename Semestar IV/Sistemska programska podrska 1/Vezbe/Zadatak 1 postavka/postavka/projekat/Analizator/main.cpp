#include "LexicalAnalysis.h"
#include "SyntaxAnalysis.h"

#include <iostream>
#include <exception>

using namespace std;

void main()
{
	try
	{
		bool retVal = false;

		LexicalAnalysis lex;
		
		if (!lex.readInputFile(".\\..\\program.txt"))
			throw runtime_error("\nException! Failed to open input file!\n");
		
		lex.initialize();

		retVal = lex.Do();

		if (retVal)
		{
			lex.printTokens();
			cout << "Lexical analysis finished successfully!" << endl;
		}
		else
		{
			lex.printLexError();
			throw runtime_error("\nException! Lexical analysis failed!\n");
		}

		SyntaxAnalysis syntax(lex);

		retVal = syntax.Do();

		if (retVal)
		{
			cout << "Syntax analysis finished successfully!" << endl;
		}
		else
		{
			throw runtime_error("\nException! Syntax analysis failed!\n");
		}
	}
	catch (runtime_error e)
	{
		cout << e.what() << endl;
	}
}