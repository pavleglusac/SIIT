package_register = {}

def add_packages():
    package_register[123] = {"code": 123, "address": "Narodnog fronta 14", "value": 1000}
    package_register[124] = {"code": 124, "address": "Neka druga adresa BB",  "value": 150}
    package_register[125] = {"code": 125, "address": "Neznanog junaka 2", "value": 7900}
    package_register[126] = {"code": 126, "address": "Narodnog fronta 14", "value": 150}
    package_register[127] = {"code": 127, "address": "Neka druga adresa BB", "value": 9700}
    package_register[128] = {"code": 128, "address": "Narodnog fronta 14", "value": 250}


def print_package_data(package):
    print("Code: %d, address: %s, value: %.2f" % tuple(package.values()))

def find_min_value():
    # Problem: Koju vrednost da dodelimo promenljivoj min_value?
    # Ne možemo da uzmemo prvu pošiljku, jer im pristupamo samo po ključu
    min_value = None
    min_package = None

    for package in package_register.values():
        # Ako smo tek počeli pretragu, prvu pošiljku na koju nailazimo proglašavamo najmanjom
        if min_value is None:
            min_value = package["value"]
            min_package = package

        # Ako min_value nije None, vrednost tretnutne pošiljke poredimo sa zabeleženom najmanjom vrednošću
        elif package["value"] < min_value:
            min_value = package["value"]
            min_package = package

        # Da izbegnemo ponavljanje koda, mogli smo da napišemo:
        # if not min_value or package["value"] < min_value:
        #     min_value = package["value"]
        #     min_package = package

    return min_package

def value_sum_by_address(address):
    sum = 0

    for package in package_register.values():
        if package["address"] == address:
            sum += package["value"]

    return sum

def value_sum_for_each_address():

    addresses = {}

    for package in package_register.values():

        current_address = package["address"]
        current_value = package["value"]

        if current_address in addresses:
            addresses[current_address] += current_value
        else:
            addresses[current_address] = current_value

    return addresses

def find_most_popular_address():
    addresses = value_sum_for_each_address()

    max_address = None
    max_value_sum = None

    for address, value_sum in addresses.items():
        if max_value_sum is None or max_value_sum < value_sum:
            max_address = address
            max_value_sum = value_sum

    return max_address, max_value_sum


if __name__ == '__main__':
    # 1
    add_packages()

    # 2
    print_package_data(find_min_value())

    # 3
    print(value_sum_by_address("Narodnog fronta 14"))

    # 4
    print(value_sum_for_each_address())

    # 5
    print(find_most_popular_address())