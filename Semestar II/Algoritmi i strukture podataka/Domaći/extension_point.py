from tokenizer import tokenize
from stack import Stack, StackError
from copy import deepcopy


class NonMatchingNumberOfParenthesisError(Exception):
    pass


class MissingOperatorError(Exception):
    pass


class UnknownCharacterError(Exception):
    pass


class MissingOperandError(Exception):
    pass


class OtherExpressionError(Exception):
    pass


# Napravio sam posebne vrste ostalih izuzetaka, ali mi test nije assertao pravilno jer ne gleda na ove kao Other..
#
# class InvalidEntryError(OtherExpressionError):
#     pass
#
#
# class OutOfPlaceDotError(OtherExpressionError):
#     pass
#
#
# class EmptyInputError(OtherExpressionError):
#     pass
#
#
# class MyZeroDivisionError(OtherExpressionError):
#     pass
#
#
# class ComplexNumberCalculationError(OtherExpressionError):
#     pass


def priority(op):
    if op == '^':
        return 3
    elif op in ('*', '/'):
        return 2
    elif op in ('+', '-', '~'):
        return 1
    elif op == '(':
        return 0


def is_number(token):
    if token.replace(".", "").replace("-", "").isnumeric():
        return True
    return False


def is_round(float_number):
    if int(float_number) == 0 and float_number != 0:
        return False
    elif float_number == 0:
        return True
    elif float_number % int(float_number) == 0:
        return True
    return False


def validate(tokens):
    par_stack = Stack()
    b = [x for x in tokens if x not in ('(', ')')]
    if not tokens:
        raise OtherExpressionError("Invalid entry error.")
        # raise InvalidEntryError
    for i in range(len(b) - 1):
        if is_number(b[i]) and is_number(b[i + 1]):
            raise MissingOperatorError('Operator missing between ' + str(b[i] + ' and ' + str(b[i+1] + '.')))
    for i in range(len(tokens) - 1):
        if tokens[i] == '(' and tokens[i + 1] in ('*', '+', '/', '^'):
            raise MissingOperandError
        if tokens [i] in ('*', '+', '/', '^', '-') and tokens[i+1] == ')':
            raise MissingOperandError
    closed_parenthesis_index = 0
    for i in range(len(tokens)):
        closed_parenthesis_index += 1
        if tokens[i] == '(':
            par_stack.push(tokens[i])
        if tokens[i] == ')':
            try:
                par_stack.pop()
            except StackError:
                raise NonMatchingNumberOfParenthesisError
    if tokens[-1] in ('-', '+', '/', '^', '*'):
        raise MissingOperandError
    if not par_stack.is_empty():
        raise NonMatchingNumberOfParenthesisError


def check_expression(ex):
    stripped_list = [x for x in list(ex) if not x == ' ']  # za zagrade
    for ch in ex:
        if not is_number(ch) and ch not in ('-', '+', '/', '^', '*', '(', ')', ' ', '.'):
            raise UnknownCharacterError('\'' + ch + '\'' + " is not a valid character.")
    for i in range(1, len(ex) - 1):
        if ex[i] != '.':
            continue
        if not (is_number(ex[i-1]) or is_number(ex[i+1])):
            raise OtherExpressionError("Out of place dot.")
            # raise OutOfPlaceDotError
    if len(ex) >= 2:
        if (ex[0] == '.' and not is_number(ex[1])) or (ex[-1] == '.' and not is_number(ex[-2])):
            raise OtherExpressionError("Out of place dot.")
            # raise OutOfPlaceDotError
    for j in range(len(stripped_list) - 1):
        if stripped_list[j] == '(' and stripped_list[j + 1] == ')':
            raise MissingOperandError
        if stripped_list[j] == '-' and stripped_list[j + 1] == '-':
            raise MissingOperandError


def split_tokens(tokens):
    new_tokens = []
    for token in tokens:
        if is_number(token):
            if float(token) < 0:
                new_tokens.append(token[0])
                new_tokens.append(token[1:])
            else:
                new_tokens.append(token)
        else:
            if len(token) > 1:
                new_tokens += list(token)
            else:
                new_tokens.append(token)
    return new_tokens


def calculate_from_infix(expression):
    if not expression.strip():
        raise OtherExpressionError("Empty input error.")
        # raise EmptyInputError
    check_expression(expression)
    tokens = to_postfix(expression)
    return calculate_from_postfix(tokens)


def to_postfix(expression):
    operators = Stack()
    output = []
    previous_token = ''

    tokens = split_tokens(tokenize(expression))  # split_tokens odvaja minuse od zagrada i brojeva
    validate(tokens)

    for token in tokens:
        if token == '-' and (previous_token == '(' or previous_token == ''):
            token = '~'
        if token in ('+', '-', '*', '/', '^', '~'):
            if operators.is_empty():
                operators.push(token)
                continue
            while priority(token) <= priority(operators.top()):
                if token != '^':
                    output.append(operators.pop())
                else:
                    break
                if operators.is_empty():
                    break
            operators.push(token)
        elif token == '(':
            operators.push(token)
        elif token == ')':
            while operators.top() != '(':
                output.append(operators.pop())
                if operators.is_empty():
                    break
            if not operators.is_empty():
                operators.pop()
        else:
            output.append(token)
        previous_token = deepcopy(token);
    while not operators.is_empty():
        output.append(operators.pop())
    return output


def calculate(first, second, operation):
    if operation == '+':
        return str(float(first) + float(second))
    if operation == '-':
        return str(float(second) - float(first))
    if operation == '*':
        return str(float(first) * float(second))
    if operation == '/':
        try:
            return str(float(second) / float(first))
        except Exception:
            raise OtherExpressionError("Attempted division by zero.")
            # raise MyZeroDivisionError("Attempted division by zero.")
    if operation == '^':
        if float(second) < 0 and not is_round(float(first)):
            raise OtherExpressionError("Attempted calculation of complex numbers.")
            # raise ComplexNumberCalculationError("I don't do that.")
        if float(second) == 0 and float(first) < 0:
            raise OtherExpressionError("Zero cannot be raised to a negative power.")
            # raise MyZeroDivisionError("Zero cannot be raised to a negative power.")
        return str(float(second) ** float(first))


def final_type(result):
    float_result = float(result)
    if is_round(float_result):
        return int(float_result)
    return float_result


def calculate_from_postfix(tokens):
    operands = Stack()
    for token in tokens:
        if token == '~':
            try:
                token = "-"
                first_operand = operands.pop()
            except Exception:
                raise MissingOperandError
            solution = calculate(first_operand, '0', token)
            operands.push(solution)
            continue
        elif token in ('+', '-', '*', '/', '^'):
            try:
                first_operand = operands.pop()
                second_operand = operands.pop()
            except Exception:
                raise MissingOperandError
            solution = calculate(first_operand, second_operand, token)
            operands.push(solution)
            continue
        else:
            operands.push(token)
    result = final_type(operands.pop())
    if not operands.is_empty():
        raise MissingOperandError
    return result


def test():
    test_cases_valid = {
        # test floats
        "3.14   ^2": 9.8596,
        "(2.08-.03) ^  2": 4.2025000000000015,

        # test integers
        "2+(3*4)": 14,
        "22*56/11": 112.0,

        # test negative
        "-(2+1)+1": -2,
        "(-4-2)+1": -5,
        "-(-5)": 5,
    }

    test_cases_invalid = {
        "22     56": MissingOperatorError,
        "ab cd": UnknownCharacterError,
        "10,22": UnknownCharacterError,
        "1-1-": MissingOperandError,
        "(43-(1+3)+3": NonMatchingNumberOfParenthesisError,
        "  ": OtherExpressionError

    }

    for expression, expected in test_cases_valid.items():
        result = calculate_from_infix(expression)
        assert expected == result
        print('radi')

    for expression, expected in test_cases_invalid.items():
        try:
            result = calculate_from_infix(expression)
            assert False
        except Exception as e:
            assert type(e) == expected


if __name__ == '__main__':
    test()
    pass
