#pragma once

#include <string>

class Variable
{

public:

	Variable() : m_name("") {}

	Variable(std::string name) : m_name(name) {}
	
	/**
	 * Set method for variable name
	 */
	void setName(std::string name) { m_name = name; }

	/**
	 * Get method for variable name
	 */
	std::string getName() { return m_name; }

private:

	/**
	 * Name of the variable
	 */
	std::string m_name;
};