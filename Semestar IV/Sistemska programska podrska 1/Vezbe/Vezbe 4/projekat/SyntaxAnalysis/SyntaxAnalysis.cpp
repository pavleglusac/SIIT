#include <iostream>
#include <iomanip>

#include "SyntaxAnalysis.h"

using namespace std;


SyntaxAnalysis::SyntaxAnalysis(LexicalAnalysis& lex)
	: lexicalAnalysis(lex), errorFound(false), tokenIterator(lexicalAnalysis.getTokenList().begin())
{
}


bool SyntaxAnalysis::Do()
{
	currentToken = getNextToken();
	
	//TO DO: Call function for the starting non-terminal symbol
	Q();

	return !errorFound;
}


void SyntaxAnalysis::printSyntaxError(Token token)
{
	cout << "Syntax error! Token: " << token.getValue() << " unexpected" << endl;
}


void SyntaxAnalysis::printTokenInfo(Token token)
{
	cout << setw(20) << left << token.tokenTypeToString();
	cout << setw(25) << right << token.getValue() << endl;
}


void SyntaxAnalysis::eat(TokenType t)
{
	if (errorFound == false)
	{
		if (currentToken.getType() == t)
		{
			cout << currentToken.getValue() << endl;
			currentToken = getNextToken();
		}
		else
		{
			printSyntaxError(currentToken);
			errorFound = true;
		}
	}
}


Token SyntaxAnalysis::getNextToken()
{
	if (tokenIterator == lexicalAnalysis.getTokenList().end())
		throw runtime_error("End of input file reached");
	return *tokenIterator++;
}


void SyntaxAnalysis::Q()
{
	eat(BEGIN);
	S();
	L();
}


void SyntaxAnalysis::S()
{
	if (currentToken.getType() == IF) {
		eat(IF);
		O();
		eat(EQEQ);
		O();
		eat(THEN);
		S();
		if (currentToken.getType() == ELSE) {
			eat(ELSE);
			S();
		}
	}
	else {
		E();
		eat(SEMI);
	}
}

void SyntaxAnalysis::O() {
	R();
	if (currentToken.getType() == PLUS) {
		eat(PLUS);
		O();
	}
	else if (currentToken.getType() == MINUS) {
		eat(MINUS);
		O();
	}
}

void SyntaxAnalysis::R() {
	if (currentToken.getType() == NUM) {
		eat(NUM);
	}
	else if (currentToken.getType() == REAL) {
		eat(REAL);
	}
	else {
		eat(ID);
		if (currentToken.getType() == MINUSMINUS) {
			eat(MINUSMINUS);
		}
		else if (currentToken.getType() == PLUSPLUS) {
			eat(PLUSPLUS);
		}
	}
}

void SyntaxAnalysis::L()
{
	if (currentToken.getType() == END) {
		eat(END);
	}
	else {
		S();
		L();
	}
}


void SyntaxAnalysis::E()
{
	eat(ID);
	if (currentToken.getType() == PLUSEQ) {
		eat(PLUSEQ);
	}
	else if (currentToken.getType() == MINUSEQ) {
		eat(MINUSEQ);
	}
	else {
		eat(EQ);
	}
	O();
}
