## Softverski obrasci i komponente

* "Najs predmet, kul su predavanja profesor je baš mica i lepo objašnjava"
* Ispit prelagan, dobijemo skriptu
* Vežbe su u pajtonu, rade se design patterni
* **JAKO** je bitno da se prate vežbe
* Projekat se radi u 4, radi se django app

## Softver nadzorno upravljačkih sistema

* "sve može iz skripte  i slajdova"
* "Opciono se može raditi prezentacija iz ovog predmeta, iskr savetujem da ko ima vremena radi to, profesor JAKO ceni, čak je na usmenom pitao one koji nisu zašto nisu, ali oopet, nije obavezno ako ne stigneš i to je 10 dodatnih bodova"
* "Na vežbama se radi C# i asistent je možda najbolji"
* Dva kolokvijuma i projekat, najgori aspekt je vreme jer je preobimno
* Fokus je na nitima za prvi a na WCF za drugi
* Fin predmet sve u svemu

## Web programiranje

* Džigibau
* Stare tehnologije, nije potrebno pratiti predavanja
* Radi se mini projekat, 5 sati kod kuće
* Projekat se radi u dvoje, postoje eliminacije a ko uradi dobro mini projekat ne mora da ih radi u terminu, može da se brani u januaru, junu, septembru
* Projekat je web shop, moze se birati vue ili jquery a node.js je zabranjen lmaooooo

## Statistika

* [**Ovako nekako**](https://youtu.be/G_rUupAmnTM)
* Predavanja su jako korisna i nije dosta ići samo na vežbe
* "Gomilu predispitnih, dva kviza na canvasu, 2 klk (koja mogu i u rokovima), usmeni koji se sastoji od kratkih zadačića i jednog teorijskog pitanja, obavezno radite domaće"
* Pola se radi na papiru pola u R-u
* Svaki čas imaš nešto

## LPRS

* "Okej predmet"
* Ima 4 domaća zadatka koji se brane na vežbama, laki su
* Klk se sastoji od teorije na SOVI, i zadatka u Quartusu
* "Mozak mi je potisnuo detalje tog predmeta"
* Oklagia najveći problem
