if __name__ == '__main__':
    n = int(input("Unesite koji član Fibonačijevog niza želite da izračunate:"))
    a = 0
    b = 1

    for number in range(n):
        temp = b
        b = a + b
        a = temp

    print("Zadati element Fibonačijevog niza ima vrednost", a)
