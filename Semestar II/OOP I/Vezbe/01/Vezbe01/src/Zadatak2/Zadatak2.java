package Zadatak2;

public class Zadatak2 
{

	public static void main(String[] args) 
	{
		double pk = 16, at = 4, bt = 6;
		double ak, ht, pt;
		ak = Math.sqrt(pk);
		System.out.println("Stranica kvadrata je: " + ak);
		ht = Math.sqrt(bt * bt - at * at / 4);
		System.out.printf("Visina trougla je: %.2f\n", ht);
		pt = bt * ht / 2;
		System.out.printf("Povrsina trougla je: %.2f\n", pt);
	}

}
