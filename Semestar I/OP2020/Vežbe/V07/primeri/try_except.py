def simple_input_exceptions():
    while True:
        str_broj = input("Unesite broj:")

        try:
            broj = int(str_broj)
            print("Uneli ste broj", broj)
            break
        except:
            print("Niste uneli broj.")

def input_exceptions():

    while True:
        str_broj = input("Unesite broj:")

        try:
            char_at_3 = str_broj[3]
            broj = int(str_broj)
            print("Uneli ste broj", broj)
            break
        except ValueError:
            print("Niste uneli broj.")
        except IndexError:
            print("Broj mora da ima više od 3 cifre.")


def main():
    try:
        rezultat = kalkulator(3, 8, '!')
        print(rezultat)
    except Exception as e:
        print(e)

def kalkulator(a, b, op):
    if op == '+':
        return a+b
    elif op == '-':
        return a-b
    elif op == '*':
        return a*b
    elif op == '/':
        if b != 0:
            return a/b
        else:
            raise ZeroDivisionError("Deljenje nulom nije dozvoljeno.")
    else:
        raise Exception("Operator \"" + op + "\" nije podržan.")


if __name__ == '__main__':
    main()
