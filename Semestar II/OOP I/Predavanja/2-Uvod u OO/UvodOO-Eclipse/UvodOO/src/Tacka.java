public class Tacka {
	/** x koordinata */
	double x;
	/** y koordinata */
	double y;

	
	Tacka() {
		this.x = 0;
		this.y = 0;
	}
	
	Tacka(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/** Racuna udaljenost dve zadate tacke 
	 *  @param a prva tacka
	 *  @param b druga tacka
	 */
	double udaljenost(Tacka b) {
		return Math.sqrt((this.x - b.x) * (this.x - b.x) + (this.y - b.y) * (this.y - b.y));
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o instanceof Tacka) {
			Tacka t = (Tacka)o;
			if (this.x == t.x && this.y == t.y) 
				return true;
			else
				return false;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		return "Tacka (" + this.x + ", " + this.y + ")";
	}
	
}
