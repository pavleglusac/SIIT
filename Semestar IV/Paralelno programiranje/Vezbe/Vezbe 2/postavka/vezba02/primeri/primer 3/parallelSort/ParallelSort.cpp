#include "tbb\parallel_sort.h"
#include <math.h>
#include <iostream>

using namespace tbb;

const int N = 100000;
double a[N];
double b[N];

void SortExample() {
	for (int i = 0; i < N; i++) {
		a[i] = sin((double)i);
		b[i] = cos((double)i);
	}
	parallel_sort(a, a + N);
	parallel_sort(b, b + N, std::greater<double>());

	printf("Sorting done!!!\n");
	getchar();
}

void main() {
	SortExample();
	std::cout << "\nNiz A\n";
	for (auto i : a)
	{
		std::cout << i << std::endl;
	}
	std::cout << "\nNiz B\n";
	for (auto i : b)
	{
		std::cout << i << std::endl;
	}
}


