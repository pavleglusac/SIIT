package Zadatak12;

import java.util.Scanner;

public class Zadatak12 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Uneti n: ");
		int n = sc.nextInt();
		System.out.println("Uneti m: ");
		int m = sc.nextInt();
		for(int i = 1; i <= n; i++)
		{
			for(int j = 0; j < m; j++)
				System.out.printf("%6d", i * i);
			System.out.println();
		}
	}
}
