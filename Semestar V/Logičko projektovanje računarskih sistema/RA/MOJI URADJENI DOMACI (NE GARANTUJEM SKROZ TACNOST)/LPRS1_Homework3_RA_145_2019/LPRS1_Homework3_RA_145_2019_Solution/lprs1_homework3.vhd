library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
-- Libraries.

entity lprs1_homework3 is
	port(
		i_clk         :  in std_logic;
		i_rst         :  in std_logic;
		i_base        :  in std_logic_vector(1 downto 0);
		o_cnt_subseq0 : out std_logic_vector(3 downto 0);
		o_cnt_subseq1 : out std_logic_vector(3 downto 0);
		o_cnt_subseq2 : out std_logic_vector(3 downto 0)
	);
end entity;


architecture arch of lprs1_homework3 is
	-- Constants.
	constant A : std_logic_vector(1 downto 0) := "00";
	constant C : std_logic_vector(1 downto 0) := "01";
	constant G : std_logic_vector(1 downto 0) := "10";
	constant T : std_logic_vector(1 downto 0) := "11";
	
	-- Signals.
	
	type tSTATE is (IDLE, G0S012, C1S01,T2S0,A2S1,A1S2,C2S2);
	
	signal s_state			: tSTATE; 
	signal s_next_state 	: tSTATE;
	
	signal s_cnt_subseq0 : std_logic_vector(3 downto 0);
	signal s_cnt_subseq1 : std_logic_vector(3 downto 0);
	signal s_cnt_subseq2 : std_logic_vector(3 downto 0);
	
	signal s_en_subseq0 	: std_logic;
	signal s_en_subseq1	: std_logic;
	signal s_en_subseq2 	: std_logic;
	
	
begin
	-- Body.
	
	-----------------------------------------------------------------------------------------------------------------------
	
	-- AUTOMAT STANJA
	
	process(i_base, s_state) begin
	
		case (s_state) is
		
			---------- IDLE ----------
			
			when IDLE =>
			
				if(i_base=G) then 
				
					s_next_state <= G0S012;
					
				else
				
					s_next_state <= IDLE;
					
				end if;
					
			---------- G0S012 ----------	
					
			when G0S012 =>
			
				if(i_base=G) then 
				
					s_next_state <= G0S012;
					
				elsif(i_base=C) then
				
					s_next_state <= C1S01;
					
				elsif(i_base=A) then
				
					s_next_state <= A1S2;
					
				else 
				
					s_next_state <= IDLE;
					
				end if;
					
			---------- C1S01 ----------	

			when C1S01 =>
			
				if(i_base=G) then 
				
					s_next_state <= G0S012;
			
				elsif(i_base=T) then 
				
					s_next_state <= T2S0;
					
				elsif(i_base=A) then
				
					s_next_state <= A2S1;
					
				else 
				
					s_next_state <= IDLE;
					
				end if;
					
			---------- T2S0 ----------
			
			when T2S0 =>
				
				if(i_base=G) then 
				
					s_next_state <= G0S012;
			
				else 
				
					s_next_state <= IDLE;
					
				end if;
			
			---------- A2S1 ----------
			
			when A2S1 =>
				
				if(i_base=G) then 
				
					s_next_state <= G0S012;
			
				else 
				
					s_next_state <= IDLE;
					
				end if;
					
			---------- A1S2 ----------
			
			
			when A1S2 =>
			
				if(i_base=G) then 
				
					s_next_state <= G0S012;
			
				elsif(i_base=C) then 
				
					s_next_state <= C2S2;
					
				else 
				
					s_next_state <= IDLE;
					
				end if;
			
			---------- C2S2 ----------
			
			when C2S2 =>
				
				if(i_base=G) then 
				
					s_next_state <= G0S012;
			
				else 
				
					s_next_state <= IDLE;
					
				end if;
				
			---------- OSTALO ----------	
					
			when others =>
				
				s_next_state <= IDLE;
					
			---------- KRAJ ----------
				
		end case;	
		
	end process;
	
	-----------------------------------------------------------------------------------------------------------------------
	
	-- PRELAZAK U SLEDECE STANJE 
	
	process (i_clk,i_rst) begin
	
		if(i_rst='1') then 								-- OPADAJUCA IVICA
		
			s_state <= IDLE;
			
		elsif(i_clk'event and i_clk='1') then	 	-- OPADAJUCA IVICA
		
			s_state <= s_next_state;
			
		end if;
		
	end process;
	
	-----------------------------------------------------------------------------------------------------------------------
	
	-- DOZVOLE BROJANJA
	
	s_en_subseq0 <= '1' when (s_state = T2S0) else '0';
	
	s_en_subseq1 <= '1' when (s_state = A2S1) else '0';
	
	s_en_subseq2 <= '1' when (s_state = C2S2) else '0';
	
	-----------------------------------------------------------------------------------------------------------------------
	
	-- 0. BROJAC ( MODUO 10 )  -> 10 - 1 = 9 -> 9 DEC = 1001 BIN
	
	process (i_clk,i_rst) begin
	
		if(i_rst='1') then
		
			s_cnt_subseq0 <= "0000";
			
		elsif(i_clk'event and i_clk='1') then
		
			if(s_en_subseq0='1') then
			
				if(s_cnt_subseq0 = "1001") then
				
					s_cnt_subseq0 <= "0000";
					
				else
				
					s_cnt_subseq0 <= s_cnt_subseq0 + 1;
					
				end if;
				
			end if;
			
		end if;
		
	end process;
	
	-----------------------------------------------------------------------------------------------------------------------
	
	-- 1. BROJAC ( MODUO 9 )	-> 9 - 1 = 8 -> 8 DEC = 1000 BIN
	
	process (i_clk,i_rst) begin
	
		if(i_rst='1') then
		
			s_cnt_subseq1 <= "0000";
			
		elsif(i_clk'event and i_clk='1') then
		
			if(s_en_subseq1='1') then
			
				if(s_cnt_subseq1 = "1000") then
				
					s_cnt_subseq1 <= "0000";
					
				else
				
					s_cnt_subseq1 <= s_cnt_subseq1 + 1;
					
				end if;
				
			end if;
			
		end if;
		
	end process;
	
	
	-----------------------------------------------------------------------------------------------------------------------
	
	-- 2. BROJAC ( MODUO 8 )	-> 8 - 1 = 7 -> 7 DEC = 0111 BIN
	
	process (i_clk,i_rst) begin
	
		if(i_rst='1') then
		
			s_cnt_subseq2 <= "0000";
			
		elsif(i_clk'event and i_clk='1') then
		
			if(s_en_subseq2='1') then
			
				if(s_cnt_subseq2 = "0111") then
				
					s_cnt_subseq2 <= "0000";
					
				else
				
					s_cnt_subseq2 <= s_cnt_subseq2 + 1;
					
				end if;
				
			end if;
			
		end if;
		
	end process;
	
	-----------------------------------------------------------------------------------------------------------------------
	
	-- IZLAZI
	
	o_cnt_subseq0 <= s_cnt_subseq0;
	
	o_cnt_subseq1 <= s_cnt_subseq1;
	
	o_cnt_subseq2 <= s_cnt_subseq2;
	
	-----------------------------------------------------------------------------------------------------------------------
	
end architecture;
