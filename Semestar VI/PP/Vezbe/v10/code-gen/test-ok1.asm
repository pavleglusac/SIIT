
main:
		PUSH	%14
		MOV 	%15,%14
		SUBS	%15,$8,%15
@main_body:
		MOV 	$5,-4(%14)
@switch_0:
		JMP	@test_0
@case_0_0:
		MOV 	$1,-8(%14)
		JMP	@exit_0
@case_0_1:
		MOV 	$5,-8(%14)
		JMP	@exit_0
@default_0:
		MOV 	$10,-8(%14)
		JMP
@exit_0
@test_0:
		CMPS 	-4(%14),$1
		JEQ	@case_0_0
		CMPS 	-4(%14),$2
		JEQ	@case_0_1
		JMP	@default_0
@exit_0:
		MOV 	-8(%14),%13
		JMP 	@main_exit
@main_exit:
		MOV 	%14,%15
		POP 	%14
		RET