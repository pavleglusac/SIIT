package Zadatak9;
import java.util.Scanner;

public class Zadatak9 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Uneti n: ");
		int n = sc.nextInt();
		System.out.println("Uneti m: ");
		int m = sc.nextInt();
		int pisi = 0;
		int suma = 0;
		for(int i = 0; i < n; i++)
		{
			pisi = i;
			for(int j = 0; j < m; j++)
			{
				if(i == j)
					suma += pisi;
				System.out.printf("%6d", pisi++);
			}
			System.out.println();
		}
		System.out.println("Suma elemenata na glavnoj dijagonali je: " + suma);
		

	}

}
