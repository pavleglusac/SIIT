let firebaseURL = "https://wd-vezbe8-default-rtdb.firebaseio.com/videos.json";

let videoPlaylist = document.getElementById("videoPlaylist");
let player = document.getElementById("player");

let request = new XMLHttpRequest();

request.onreadystatechange = function(event){
    if(this.readyState === 4 ){
        if(this.status === 200){


            let videos = JSON.parse(this.responseText);
            for(let i=-0;i<videos.length;i++){
               
                console.log(videos[i])

                let videoLi = document.createElement("li");

                let ordinalSpan = document.createElement("span");
                ordinalSpan.classList.add("orderNumber");
                ordinalSpan.innerText = i+1;
                videoLi.appendChild(ordinalSpan);

                let titleSpan = document.createElement("span");
                titleSpan.classList.add("songSpan");
                titleSpan.innerText = videos[i].title;
                videoLi.appendChild(titleSpan);

                videoLi.setAttribute("data-url",videos[i].link)

                videoLi.addEventListener("click",function(e){

                    let titleElement = document.getElementById("titleElement");
                    titleElement.innerText = this.querySelector(".songSpan").innerText;
                    

                    let link = this.getAttribute("data-url");
                    player.setAttribute("src",link);
                    player.play();

                })

                videoPlaylist.appendChild(videoLi);
            }


        }else{
            alert('Greska prilikom preuzimanja podataka.');
        }
    }
}

request.open('GET',firebaseURL);
request.send();