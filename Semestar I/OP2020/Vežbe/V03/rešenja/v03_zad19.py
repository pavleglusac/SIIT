if __name__ == '__main__':
    n = int(input("Unesite koliko članova niza želite da saberete:"))
    sum = 0
    sign = 1
    dividend = 4
    divisor = 1

    for number in range(n):
        sum += sign*dividend/divisor
        sign *= -1
        divisor += 2

    print("Broj pi iznosi približno", sum)
