if __name__ == '__main__':
    diff = eval(input("Unesite vremensku razliku između trenutka pojavljivanja munje i trenutka kada zvuk stigne"
                      " do posmatrača:"))
    speed = 340
    distance = speed * diff
    print("Udaljenost posmatrača od munje je", distance, "m.")