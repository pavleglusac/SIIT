**Fajlovi potrebni za izradu projekta su u folderu Projekat 2**

Potrebno je napraviti mašinu za pretraživanje tekstualnih dokumenata (search engine). Program prilikom startovanja treba da obiđe stablo direktorijuma u fajl sistemu počevši od datog korena, da parsira html fajlove (pomoću datog modula parser) u njima i da izgradi strukture podataka potrebne za efikasno pretraživanje. Nakon toga, program omogućuje korisniku da unosi tekstualne upite koji se sastoje od **jedne ili više reči** razdvojenih razmakom, pretražuje dokumente koristeći prethodno kreiranu strukturu podataka i korisniku ispisuje rangirane rezultate pretrage. Pretraga bi trebalo da bude neosetljiva na razliku u veličini slova (case insensitive).

**Napomene:**

1.  Potrebno je posmatrati samo HTML fajlove unutar test skupa.
2.  Rezultati treba da sadrže redni broj rezultata, naziv stranice kao i kratak kontekst (isečak iz dela stranice) u kome je tražena reč pronađena. Poželjno je da tražena reč (ili više njih) bude označena u isečku (npr. bojenjem konzolnog ispisa).  
      
    

Za maksimalnih 10 poena:
------------------------

*   Implementirati rangiranje rezultata pretrage tako da na rang rezultata utiče broj pojavljivanja traženih reči na stranici uz korišćenje proizvoljnih struktura podataka.
*   Ukoliko korisnik unosi upit sastavljen od više reči, rangiranje stranica po svakoj pojedinačnoj reči utiče na sveukupno rangiranje određene stranice. U ovom slučaju, ne treba insistirati na prisustvu svake od reči u rezultatima, ali bi trebalo bolje rangirati rezultate u kojima se pojavljuju sve reči.

Za više od 10 poena:
--------------------

*   Na rangiranje, osim broja pojavljivanja traženih reči u dokumentu, treba utiče i broj linkova iz drugih dokumenata na pronađeni dokument, kao i broj traženih reči u dokumentima koji sadrže link na traženi dokument (4 poena)
*   Za organizovanje stranica koristiti graf. Za efikasnu pretragu reči na stranici koristiti strukturu podataka Trie. (5 poena)
*   Potrebno je podržati ispravnu upotrebu logičkih operatora AND, OR i NOT prilikom formiranja upita uz kombinovanje operatora. (4 poena)  
    Primeri:  
    python AND sequence  
     dictionary NOT list  
     python OR dictionary NOT word  
      
    
*   Obezbediti paginaciju rezultata. Ograničiti broj ispisa po stranici na N (rezultati od 1 do N) uz nuđenje opcije za ispis sledećih N rezultata (od N+1 do 2N) itd. (1 poen)

Za više od 24 poena:
--------------------

*   Upotreba fraza. Fraza se navodi pod navodnicima. U rezultatima se prikazuju (uz rangiranje) stranice u kojima se navedeni delovi fraze pojavljuju uzastopno u istom redosledu (2 poena)
*   Implementacija predlaganja alternativnih ključnih reči za pretragu _(did you mean)_. Ukoliko rezultata pretrage nema (nema rangiranih stranica) ili se zadati upit pojavljuje na malom broju stranica, ponuditi korisniku da zadati upit zameni sličnim, popularnijim upitom. Na primer, ako korisnik unese reč 'pyhton' koje nema ni na jednoj stranici, trebalo bi da mu se ponudi slična reč 'python' koja je mnogo zastupljenija (2 poena)
*   _autocomplete_ - odabirom ove opcije korisniku se nudi nekoliko popularnih završetaka zadatog upita, npr. ako korisnik unese _di__\*_ ponuđene opcije mogu biti _dictionary_ i _different_ (2 poena)

Druga unapređenja funkcionalnosti koje predmetni profesor ili asistent odobre mogu doneti do 5 dodatnih poena.

Dodatna objašnjenja
-------------------

Logički operator AND zahteva prisustvo svih navedenih reči u stranicama koje su u rešenju. Sve navedene reči bi trebalo u jednakoj meri da utiču na ukupan rang.

Logički operator OR zahteva prisustvo bar jedne od navedenih reči u stranicama koje predstavljaju rešenje.

Logički operator NOT se u ovom slučaju smatra BINARNIM dakle, predviđa se upotreba u obliku 'reč1 NOT reč2' gde se pojavljivanje reči _reč1_ zahteva u rešenju dok se _reč2_ u rešenju ne sme pojaviti.

Opšte informacije o slanju i upload-u zadatka:

*   Zadatak nosi 30 poena.
*   Smestiti sve fajlove zadatka u **folder** pod nazivom projekat2\_sw\_XX\_YYYY gde se umesto XX\_YYYY navodi broj indeksa - broj upisa i godina upisa (primer: projekat2\_sw\_02\_2019)
*   Ubaciti fajl u **zip** arhivu i nazvati je isto kao i zadatak (projekat2\_sw\_XX\_YYYY.zip)
*   Uploadovati zip arhivu kao assignment na enastavu.
*   Ukoliko bude problema sa uploadom, možete u predviđenom roku poslati zip na email adresu predmetnog asistenta.

Rok za slanje arhive je četvrtak 25.06.2020.