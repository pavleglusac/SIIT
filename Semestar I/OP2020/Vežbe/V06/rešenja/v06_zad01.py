def calculate_income(file_path, hourly_wage):
    file = open(file_path, "r")
    lines = file.readlines()

    for line in lines:
        line = line.replace("\n", "")
        worker_data = line.split("|")

        name = worker_data[0]
        print("Ime:", name)

        weekly_hours = 0

        for daily_hours in worker_data[1:]:
            weekly_hours += int(daily_hours)

        income = hourly_wage * weekly_hours

        if weekly_hours > 40:
            income = income*1.5

        print("Zarada:", income)


    file.close()

if __name__ == '__main__':
    calculate_income("../fajlovi/radnici.txt", 1000)