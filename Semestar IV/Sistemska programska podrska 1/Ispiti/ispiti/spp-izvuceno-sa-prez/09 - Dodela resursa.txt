9.1 DODELA RESURSA, BOJENJE GRAFA

* Faza dodele resursa

- Ova faza se u literaturi naziva i fazom dodele registara, pri cemu se pod registrima podrazumeva skup resursa istog tipa
- Dodela registara promenljivama iz grafa smetnji se obavlja tzv. bojenjem grafa smetnji

* Bojenje grafa smetnji

- Svakom registru odredisnog procesora odgovara jedna boja, tako da se kod procesora sa K registara, graf smetnji boji sa K boja, sto se naziva K-bojenje
- Ako je K-bojenje nekog grafa smetnji uspesno, ono odredjuje ispravnu dodelu registara privremenim promenljivama
- U suprotnom slucaju, neke privremene promenljive se moraju smestiti u neku drugu klasu resursa (npr. memorijske lokacije), sto se naziva prelivanjem (spill)

* Rang cvora i Znacajni rang

- Dva cvora grafa smetnje su susedna ukoliko su medjusobno povezana (tj. ako izmedju njih postoji smetnja)
- Rang cvora grafa smetnji je jednak broju njemu susednih cvorova
- Rang cvora koji je veci ili jednak K (br. raspolozivih boja) je znacajan rang
- Za cvor ciji rang je znacajan se kaze da je znacajan cvor

* Osnovne primitive (faze) algoritma za dodelu resursa
	- Formiraj: formiraj graf smetnji
	- Uprosti: cvor koji nije znacajan se izvlaci iz grafa i gura na stek
	- Prelij: primenjuje u situaciji kad su svi preostali cvorovi znacajni. On izabere jedan znacajan cvor, oznaci ga kao kandidata za prelivanje, izvuce iz grafa i gurne na stek
	- Izaberi: 1) Dodeljuje boju cvoru sa steka, koji se potom skida sa steka i vraca u graf koji se postepeno rekonstruise
		   2) Ukoliko nema boje, cvor se preliva
	- Ponovi: ukoliko je doslo do prelivanja, cvor je smesten u resurs druge klase.


- Graf smetnji se koristi i za uklanjanje nepotrebnih MOVE instrukcija
- Ukoliko izmedju izvorista i odredista MOVE instrukcije ne postoji luk grafa smetnji, MOVE instrukcija se moze ukloniti, a odg. cvorovi spojiti u novi cvor ciji lukovi su unija lukova cvorova koje zamenjuje

* Strategija Brigsa. 
- Cvorovi a i b se mogu spojiti ukoliko rezultantni cvor ab ima manje od K znacajnih suseda

* Strategija Dzordza
- Cvorovi a i b se mogu spojiti ukoliko svaki sused cvora a, ima smetnju sa b, ili nema znacajan rang

* Faza preimenovanja registara

- Faza dodele resursa radi na dva nivoa:
	- Na nivou funkcije
		- na nivou funkcije se dodeljuju akumulatori i registri opste namene za parametre funkcija, stek i registar povratne adrese
	- Na nivou modula
		- na nivou modula se dodeljuju preostali registri opste namene i memorija

