
/*
	A is coded with "00".
	C is coded with "01".
	G is coded with "10".
	T is coded with "11".
	
	Subsequence to find: CGT
*/

/*
	// Pseudo-code:
	short N_bases = 175;
	short* p_sequence_rom = 0x1600;
	short* p_subseqs = 0x0170;
	short* p_done = 0x0200;
	short subsequence[3] = {1, 2, 3};

*/
.data
175				// 0 N_bases
0x1600 			// 1 p_sequence_rom
0x0170			// 1 p_subseqs
0x0200			// 3 p_done
1, 2, 3 		// 4 CGT

/*
	// Pseudo-code:
	short ssi = 0; // ssi - subsequence index.
	for(short bi = 0; bi != N_bases; bi++){ // bi - base index.
		short seq_base = p_sequence_rom[bi];
		short subseq_base = subsequence[ssi];
		if(seq_base != subseq_base){
			ssi = 0; // Start from beginging of subsequence.
		}else{
			// Base match.
			ssi++;
			if(ssi == 3){
				// Found subsequence.
				ssi = 0; // Start new subsequence.
				*p_subseqs++; // Increment subsequence counter.
			}
		}
	}
	*p_done = 1;
*/
.text
/*
	Recommended register list:
	R0 - tmp N_bases, addr
	R1 - seq_base
	R2 - subseq_base
	R3 - bi
	R4 - ssi
	R5 - p_sequence_rom
	R6 - subsequence
	R7 - p_subseqs
*/
#define ZERO(x)  sub x, x, x

begin:
//TODO Sequencer code.
	ZERO(R0) 			//R0 = 0
	ZERO(R1) 			//R1 = 0
	ZERO(R2) 			//R2 = 0
	ZERO(R3) 			//R3 = 0, bi=0
	ZERO(R4) 			//R4 = 0
	ZERO(R5) 			//R5 = 0
	ZERO(R6) 			//R6 = 0
	ZERO(R7) 			//R7 = 0
	
	
	
	inc R0, R0			//R0++, R0 = 1
	ld R5, R0			//R5 = *(R0)   UCITAVAMO SADRZAJ SA PRVE ADRESE TJ UBACUJEMO 0x1600 U R5, ODATLE CITAMO VELIKU SEKVENCU
	inc R0, R0			//R0++, R0 = 2
	ld R7, R0			//R7 = *(R0)   UCITAVAMO SADRZAJ SA DRUGE ADRESE TJ UBACUJEMO 0x0170 U R7, TU UPISUJEMO REZULTAT, TJ BROJ PRONADJENIH PODSEKVENCI
	ZERO(R0) 			//R0 = 0
	st R0, R7			//STAVLJANJE 0 NA BROJAC PODSEKVENCI
	jmp podsekvenca
	
sledeci:
	ZERO(R4) 			//R4 = 0, ssi=0
	ZERO(R1)				//R1 = 0
	ld R0,R1     		//R0 = *(R1) U R0 UBACUJEMO BROJ 175, JER SE ON NALAZI NA NULTOJ MEMORIJSKOJ ADRESI, PA ONDA I SMANJENJE BROJEVE KAKO KROZ PROGRAM PROLAZIMO
	dec R0, R0			//R0-- ,R0 = 174, POLAKO OPADA BROJ
	jmpz done			//PROVERA DA LI SMO PRESLI CELU SEKVENCU 
	st R0, R1			//NA NULTU MEMORIJSKU LOKACIJU (JER JE R1 = 0) UPISUJE SAD 174 , POLAKO OPADA BROJ
	ZERO(R1)				//R1 = 0
	jmp podsekvenca
	
sledeci_skv:
	add R1, R1, R4    // POVECAVANJE SSI, 
	
podsekvenca:
	inc R1, R1			//R1 = 1
	inc R1, R1			//R1 = 2
	inc R1, R1			//R1 = 3
	inc R1, R1			//R1 = 4
	
vrednost:
	ld R6, R1   		//U R6 SADA UCITAVAMO PODATKE SA CETVRTE (R1=4) MEMORISJKE LOKACIJE TJ POCETAK NIZA BAZA , TJ PRVO SLOVO (NA 5 I 6 SE NALAZE OSTALA 2 SLOVA)	

petlja:
	ld R1, R5      	//U R1 SAD UCITAVAMO VREDNOST KOJA SE NALAZI U REGISTRU NA KOJI POKAZUJE R5, TJ PRVO SLOVO SEKVENCE VELIKE
	inc R5, R5	 		//R5++ , POVECAVAMO BROJAC ZA SLEDECE SLOVO U VELIKOJ SEKVENCI
	sub R1, R1, R6 	//POREDIMO SLOVA KOJA SE NALAZE U U REGISTRIMA R6 I R1, AKO NISU NULA ZNACI RAZLICITI SU I IDEMO NA SLEDECE SLOVO VELIKE SEKVENCE
	jmpnz sledeci	
	inc R4, R4	   	//PRELAZIMO NA SLEDECE SLOVO U PODSEKVENIC, TJ POVECAVAMO INDEX 
	ZERO(R1)       	//R1 = 0
	inc R1, R1			//R1 = 1
	inc R1, R1			//R1 = 2
	inc R1, R1			//R1 = 3
	sub R1, R1, R4		//3 - R4, AKO IMAMO BROJ 3 U R4 (znaci nasli smo 0 1 2 slovo) ONDA SMO NASLI PODSEKVENCU, AKO NEMAMO ONDA PRESKACEMO JUMP
	jmpz nasao
	
dalje:	
	ZERO(R1)				//R1 = 0
	ld R0,R1     		//UCITAVAMO 175 ILI NEKI DRUGI BROJ
	dec R0, R0			//SMANJUJEMO 175 I NEKI DRUGI BROJ I GLEDAMO DA LI SMO DOSLI DO 0 ZBOG SMANJIVANJA
	jmpz done			//AKO JESMO SKACEMO NA KRAJ I TAMO RADIMO DALJE PO ZADATKU, AKO NISMO IDEMO DALJE
	st R0, R1			//NA NULTU MEMORIJSKU LOKACIJU (JER JE R1 = 0) UPISUJE SAD 174 , POLAKO OPADA BROJ
	jmp sledeci_skv	//IDEMO DA PROVERAVAMO DA LI NAREDNA SLOVA U SEKVENCI ODGOVARAJU NAREDNIMA U PODSEKVENCI
	
nasao:
	ZERO(R4)				//R4=0
	ld R1, R7			//UCITAVA U R1 VREDNOST IZ R7 (BROJ PRONADJENIH PODSEKVENCI)
	inc R1, R1			//POVECAVA BROJ PRONADJENIH PODSEKVENCI
	st R1, R7			//SKLADISTI TAJ NOVI BROJ NAZAD U REGISTAR NA KOJI POKAZUJE R7
	jmp dalje
	
done:	
	ZERO(R1)				//R1 = 0
	inc R1, R1			//R1 = 0
	inc R1, R1			//R1 = 1
	inc R1, R1			//R1 = 2
	ld R0,R1				//UCITAVA ADRESU DONE REGISTRA
	ZERO(R1)				//R1 = 0
	inc R1,R1			//R1 = 1
	st R1, R0 			//UPISIVANJE BROJA 1 U DONE
	
end:	
	jmp end    			//infinite loop
