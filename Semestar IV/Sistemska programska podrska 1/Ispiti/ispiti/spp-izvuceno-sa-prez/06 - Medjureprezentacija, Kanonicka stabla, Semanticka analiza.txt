6.1 PRAVLJENJE SEMANTICKOG ANALIZATORA

* Semanticki analizator

- Popunjava tabele simbola
- Obrada deklaracija tipova, prom. i funkcija
- Obrada koriscenja (pojave) identifikatora

6.2 KOMPAJLER - II DEO

* Faza prevodjenja u medjukod (intermediate representation)

- Stablo sintaksne analize se u ovoj fazi prevodi u stablo medjukoda (IR)
- IR nije vezan ni za odredjeni programski jezik, niti za odredjenu ciljnu platformu
- Po svom obliku medjukod najvise podseca na asembler nekog hipotetickog procesora

- Centralna osobina medjukoda je njegova nezavisnost od:
	- Izvornog koda (programskog jezika)
	- Ciljnog procesora

- Dobar medjukod zadovoljava sledece osobine:
	- Pogodan je za davanje znacenja cvorovima stabla sintaksne analize
	- Pogodan je za prevodjenje u masinski jezik bilo kog odredisnog procesora
	- Cvorovi stabla medjukoda treba da imaju jasna i jednostavna znacenja

- Problem prevodjenja
	- Cvorovi stabla sintaksne analize opisuju slozene operacije polaznog programa
		- Indeksiranje niza, poziv funkcije..
	- Problem: izmedju slozenih operacija stabla sintaksne analize i slozenih masinskih instrukcija obicno nema direktnog preslikavanja

- Nivo apstrakcije medjukoda
	- Najcesce: jedan cvor stabla sintaksne analize - veci broj cvorova medjukoda
	- Cvorovi stabla medjukoda su elementarni izrazi i iskazi

6.3 KANONIZACIJA MEDJUKODA (svodjenje na osnovni oblik)

- Neki aspekti medjukoda ne odgovaraju masinskom jeziku, a neki drugi aspekti ometaju optimizaciju medjukoda
- Poseban problem su ESEQ i CALL cvorovi koji sadrze iskaze dodele i obavljaju U/I operacije (to se naziva ivicnim efektima)
- Ivicni efekti dovode do toga da se podizrazi nekog izraza ne mogu izracunavati u bilo kom redosledu
- Problemi se eliminisu transformacijom medjukoda, gde se stablo pretvara u listu tzv. kanonickih stabala, koja nemaju ni SEQ ni ESEQ cvorova ( a nemaju ni CALL cvorove kojima je CALL predak)

Kanonicka stabla medjukoda
- Dve osobine:
	- ne sadrzi SEQ i ESEQ
	- predak CALL cvora je cvor EXP ili MOVE

- Osnovni blok - se definise kao niz susednih iskaza koji:
	- Zapocinje labelom LABEL,
	- Zavrsava se iskazom skoka JUMP ili CJUMP
	- Unutar bloka ne postoji LABEL, JUMP ili CJUMP

- U cilju odredjivanja osnovnih blokova programa analizira se tok upravljanja
	- Instrukcija uslovnog grananja
	- Nizovi instrukcija izmedju instrukcija grananja se grupisu u blokove

- Odredjivanje osnovnih blokova i tragova
	- IR stablo se najpre podeli na osnovne blokove
	- Oni se zatim rasporedjuju po programskim tragovima, tako da se dobije kod sa sto manje instrukcija skoka
	- Svaki osnovni blok je pokriven iskljucivo jednim tragom

- Tragom se naziva niz iskaza koji se mogu uzastopno izvrsavati tokom izvrsenja programa
- Trag moze sadrzati i uslovne skokove
- Od interesa je minimalan skup tragova koji se medjusobno ne preklapaju, a u potpunosti prekrivaju program







