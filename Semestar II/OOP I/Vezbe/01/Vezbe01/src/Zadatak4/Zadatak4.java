package Zadatak4;

import java.util.Scanner;

public class Zadatak4 {

	public static void main(String[] args) {
		System.out.println("Uneti rastojanje u cm: ");
		var sc = new Scanner(System.in);
		double cm, m, dm;
		cm = sc.nextDouble();
		m = cm / 100;
		dm = cm / 10;
		System.out.printf("%.2f cm = %.2f m = %.2f dm", cm, m, dm);
		sc.close();

	}

}
