package nasledjivanje;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Automobil extends Vozilo {
	public Automobil() {
		// super(); // ovo ne moramo
		System.out.println("Konstruktor Automobila");
	}

	public Automobil(String s) throws IOException {
		// super(s);
		System.out.println("Konstruktor Automobila sa parametrom: " + s);
		BufferedReader in;
		in = new BufferedReader(new FileReader("pera.txt"));

		in.close();

		new Unutrasnja();
	}

	static class Unutrasnja {

	}
}
