from v06_zad08 import is_leap

def check_date(date):
    parts = date.split("/")
    day = int(parts[0])
    month = int(parts[1])
    year = int(parts[2])

    if month < 1 or month > 12 or day > 31 or day < 1:
        return False

    if month in [4, 6, 9, 11]:
        if day == 31:
            return False

    if month == 2:
        if day > 29:
            return False
        if not is_leap(year) and day > 28:
            return False

    return True


if __name__ == '__main__':
    print(check_date("15/04/2000"))
    print(check_date("28/02/2001"))
    print(check_date("29/02/2001"))
    print(check_date("29/02/2000"))
    print(check_date("30/02/2000"))
    print(check_date("31/06/2001"))
    print(check_date("31/05/2001"))
    print(check_date("37/06/2001"))
    print(check_date("32/08/2001"))
    print(check_date("37/14/2001"))