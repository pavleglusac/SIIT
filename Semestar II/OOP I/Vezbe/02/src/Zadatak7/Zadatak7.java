package Zadatak7;

public class Zadatak7 
{
	public static void main(String[] args) 
	{
		int niz[] = {-10, 3, 16, 1, 4, -2};
		int max = niz[0];
		int min = niz[0];
		double srednjaVrednost = 0;
		for(int el: niz)
		{
			srednjaVrednost += el;
			if(el > max)
				max = el;
			if(el < min)
				min = el;
		}
		System.out.printf("a) Najveci element niza je %d, a najmanji element je %d\n", max, min);
		System.out.printf("b) Srednja vrednost niza je %.2f\n", srednjaVrednost/niz.length);
		System.out.println("c) Svi pozitivni elementi manji od srednje vrednosti niza su: ");
		for(int el: niz)
		{
			if(el > 0 && el < srednjaVrednost/niz.length)
				System.out.println(el);
			
		}
	}
}
