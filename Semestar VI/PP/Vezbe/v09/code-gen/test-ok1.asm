
main:
		PUSH	%14
		MOV 	%15,%14
		SUBS	%15,$8,%15
@main_body:
		MOV 	$11,-4(%14)
		MOV 	$2,-8(%14)
		CMPS 	-4(%14),-8(%14)
		JNE 	@false_0
@true_0:
		MOV 	-4(%14),%0
		JMP	@exit_0
@false_0:
		MOV 	$0,%0
@exit_0:
		MOV 	%0,-4(%14)
		MOV 	-4(%14),%13
		JMP 	@main_exit
@main_exit:
		MOV 	%14,%15
		POP 	%14
		RET