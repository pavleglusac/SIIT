def nanny(start_time, end_time):
    start_time = start_time.split(":")
    start_h = int(start_time[0])
    start_m = int(start_time[1])
    start_moment = start_h*60+start_m

    end_time = end_time.split(":")
    end_h = int(end_time[0])
    end_m = int(end_time[1])
    end_moment = end_h*60+end_m

    moment_21h = 21*60

    if start_h < 21:
        if end_h < 21:
            rate = 150 * (end_moment - start_moment)
        else:
            rate = 150 * (moment_21h - start_moment) + \
               100 * (end_moment - moment_21h)
    else:
        rate = 100 * (end_moment - start_moment)

    # obračun je vršen po minutima, delimo na sate
    rate /= 60

    return "Zarada dadilje je %.0f dinara. " % rate


if __name__ == '__main__':
    print(nanny('18:35', '22:50'))