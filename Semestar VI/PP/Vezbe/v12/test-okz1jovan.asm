
main:
		PUSH	%14
		MOV 	%15,%14
		SUBS	%15,$8,%15
@main_body:
		MOV 	$1,-4(%14)
		MOV 	$0,-8(%14)
@first_0:
		CMPS 	-4(%14),$1
		JNE	@second_0
		ADDS	-4(%14),-4(%14),%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_0
@second_0:
		CMPS 	-4(%14),$3
		JNE	@third_0
		SUBS	-4(%14),-4(%14),%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_0
@third_0:
		CMPS 	-4(%14),$5
		JNE	@otherwise_0
		ADDS	-4(%14),$0,%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_0
@otherwise_0:
		ADDS	-4(%14),$1000,%0
		MOV 	%0,-4(%14)
@branch_end_0:
		ADDS	-4(%14),-8(%14),%0
		MOV 	%0,-8(%14)
		MOV 	$3,-4(%14)
@first_1:
		CMPS 	-4(%14),$1
		JNE	@second_1
		ADDS	-4(%14),-4(%14),%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_1
@second_1:
		CMPS 	-4(%14),$3
		JNE	@third_1
		SUBS	-4(%14),-4(%14),%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_1
@third_1:
		CMPS 	-4(%14),$5
		JNE	@otherwise_1
		ADDS	-4(%14),$0,%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_1
@otherwise_1:
		ADDS	-4(%14),$1000,%0
		MOV 	%0,-4(%14)
@branch_end_1:
		ADDS	-4(%14),-8(%14),%0
		MOV 	%0,-8(%14)
		MOV 	$5,-4(%14)
@first_2:
		CMPS 	-4(%14),$1
		JNE	@second_2
		ADDS	-4(%14),-4(%14),%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_2
@second_2:
		CMPS 	-4(%14),$3
		JNE	@third_2
		SUBS	-4(%14),-4(%14),%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_2
@third_2:
		CMPS 	-4(%14),$5
		JNE	@otherwise_2
		ADDS	-4(%14),$0,%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_2
@otherwise_2:
		ADDS	-4(%14),$1000,%0
		MOV 	%0,-4(%14)
@branch_end_2:
		ADDS	-4(%14),-8(%14),%0
		MOV 	%0,-8(%14)
		MOV 	$0,-4(%14)
@first_3:
		CMPS 	-4(%14),$1
		JNE	@second_3
		ADDS	-4(%14),-4(%14),%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_3
@second_3:
		CMPS 	-4(%14),$3
		JNE	@third_3
		SUBS	-4(%14),-4(%14),%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_3
@third_3:
		CMPS 	-4(%14),$5
		JNE	@otherwise_3
		ADDS	-4(%14),$0,%0
		MOV 	%0,-4(%14)
		JMP	@branch_end_3
@otherwise_3:
		ADDS	-4(%14),$1000,%0
		MOV 	%0,-4(%14)
@branch_end_3:
		ADDS	-4(%14),-8(%14),%0
		MOV 	%0,-8(%14)
		MOV 	-8(%14),%13
		JMP 	@main_exit
@main_exit:
		MOV 	%14,%15
		POP 	%14
		RET