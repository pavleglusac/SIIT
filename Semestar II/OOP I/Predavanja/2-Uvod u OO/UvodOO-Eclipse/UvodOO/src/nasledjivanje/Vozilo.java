package nasledjivanje;

public class Vozilo {

	public Vozilo() {
		System.out.println("Konstruktor Vozila");
	}
	
	public Vozilo(String s) {
		System.out.println("Konstruktor Vozila sa parametrom: " + s);
	}
}
