from math import sqrt

if __name__ == '__main__':
    x = eval(input("Unesite broj za koji želite da izračunate koren:"))
    n = int(input("Unesite broj pokušaja za pogađanje korena broja:"))

    guess = x/2
    for number in range(n):
        guess = (guess+x/guess)/2

    print("Njutnovom metodom odredili smo da koren broja", x, "iznosi", guess)
    print("Koren broja", x, "pomoću funkcije sqrt iznosi", sqrt(x))

