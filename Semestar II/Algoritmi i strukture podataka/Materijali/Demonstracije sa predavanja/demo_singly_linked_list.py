class EmptyListException(Exception):
    pass

class Node(object):

    def __init__(self, data, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)

class SinglyLinkedList(object):

    def __init__(self, head=None):
        self._head = head
        self._tail = head

    def add_first(self, data):
        node = Node(data, self._head)
        self._head = node

    def add_last(self, data):
        node = Node(data)

        if self._tail is None:
            self._head = node
            self._tail = node

        else:
            self._tail.next = node
            self._tail = node

    def add_last_no_tail(self, data):
        node = Node(data)

        if self._head is None:
            self._head = node

        else:
            current = self._head
            while current.next is not None:
                current = current.next

            current.next = node

    def remove_first(self):
        if self._head is None:
            raise EmptyListException("List is empty.")

        self._head = self._head.next
        #TODO update tail

    def remove_last(self):
        if self._head is None:
            raise EmptyListException("List is empty.")

        current = self._head
        while current.next is self._tail:
            current = current.next

        self._tail = current
        self._tail.next = None




    def __iter__(self):
        current = self._head
        while current is not None:
            yield current
            current = current.next

if __name__ == '__main__':

    lista = SinglyLinkedList()

    lista.add_first("A")
    lista.add_first("B")
    lista.add_first("C")
    lista.add_first("D")

    lista.add_last("M")

    for elem in lista:
        print(elem)
