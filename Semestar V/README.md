## Generalne stvari
##### Besplatne knjige
- [grupisane po jezicima](https://github.com/EbookFoundation/free-programming-books/blob/master/books/free-programming-books-langs.md)

- [grupisane po temama](https://github.com/EbookFoundation/free-programming-books/blob/master/books/free-programming-books-subjects.md)

# SEMESTAR V - Empire strikes back

- [osnovne informacije o predmetima (iskustva)](Semestar%20V/Info.md)

## Softverski obrasci i komponente

| Obaveza | Bodovi |
| ------------- | :-------------: |
| Praktični deo | **60** |
| Teorija | **40** |

#### Projekat (60b)
 - timovi 4 ± 1

| Aktivnost  | Bodovi |
| ------------- | :-------------: |
| Model komponenti | 10 |
| Implementacija komponenti | 20 |
| Implementacija projekta | 18 |
| Git aktivnost | 6 |
| GitLab aktivnost | 6 |

## Softver nadzorno upravljačkih sistema

| Obaveza | Datum | Vreme | Bodovi |
| ------------- | :-------------: | :-------------: | :-------------: |
| **Kolokvijum I** | 27-28. Nov | po terminima | **20** |
| **Kolokvijum II** | 10-13. Jan | * | **20** |
| **Projekat** | TBD | TBD | **30** |
| **Usmeni** | TBD | TBD | **30** |

#### Kolokvijum II
 - [termini*](https://docs.google.com/spreadsheets/d/1ustuxqc8D_rWbaZf81xLO8k1pr_0sdHGU4hEQ7fiUO4/edit#gid=702525659)

## Web programiranje

| Obaveza | Datum | Vreme | Bodovi |
| ------------- | :-------------: | :-------------: | :-------------: |
| **Mini-projekat** | 16. Jan | 10:00-15:00 | **30** |
| **Projekat** |  5. Feb | TBD | **40** |
| **Usmeni** | TBD | TBD | **30** |

#### Projekat
 - obavezan
 - uključuje eliminacije pre odbrane
 - minimalno 25b
 - termini su januar, jun, septembar

#### Usmeni
 - obavezan
 - projekat je uslov za usmeni
 - obavezan
 - minimalno 15b

## Statistika

| Obaveza | Datum | Vreme | Bodovi |
| ------------- | :-------------: | :-------------: | :-------------: |
| **Pismeni I** | 4. Dec | 19:00 | **30** |
| **Pismeni II** | TBD | TBD | **20** |
| **Test R1** | 18. Dec | 8:30 | **15** |
| **Test R2** | 15. Jan | 13:30 | **15** |
| **Usmeni** | TBD | TBD | **10+10** |

#### Pismeni
 - **minimum** na prvom pismenom je 10b, minimum na drugom je 8b

#### Računarski testovi
 - **predispitne obaveze**
 - bodovi važe **trajno**

#### Usmeni
 - sastoji se iz testa (10b) i teorije (10b)

## LPRS

 - [**tabela datuma predavanja, vežbi i ostalih obaveza**](https://docs.google.com/spreadsheets/d/1WXY5PaE-AwHtd_hDu4lXC6puyUSUotjwDiMWXYOGJrA/edit#gid=0)

| Obaveza | Bodovi |
| ------------- | :-------------: |
| Vežbe | **20** |
| Testovi | **30** |
| Ispit | **50** |

#### Predispitne obaveze
##### Vežbe
 - **pokazne**  8 x 0.5b
 - **zadaci** (za ocenu) 4 x 4b

 ##### Testovi
 - **teorija** 15b
 - **zadaci** 15b

 #### Ispit
 - **teorija** 20b
 - **zadaci** 30b
