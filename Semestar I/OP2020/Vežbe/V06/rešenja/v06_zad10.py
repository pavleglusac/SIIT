from v06_zad08 import is_leap


def day_number(date):
    parts = date.split("/")
    day = int(parts[0])
    month = int(parts[1])
    year = int(parts[2])

    day_of_year = 31*(month - 1) + day

    if month > 2:
        day_of_year -= (4*month + 23)//10

        if is_leap(year):
            day_of_year += 1

    return day_of_year


if __name__ == '__main__':
    print(day_number("02/02/2019"))
    print(day_number("12/12/2019"))
    print(day_number("27/05/2019"))