public class Krug {
	/** Centar kruga */
	Tacka centar;
	/** Polupre�nik kruga */
	double r;

	Krug() {
		this.centar = new Tacka(); // 0, 0
		this.r = 0;
	}
	
	Krug(Tacka centarKruga, double r) {
		this.centar = centarKruga;
		this.r = r;
	}

	Krug(double x, double y, double r) {
//		this.centar = new Tacka(x, y);
		this();
		this.centar.x = x;
		this.centar.y = y;
		this.r = r;
	}

	/**
	 * Ispisuje da li je neka ta�ka unutar, izvan ili na krugu, na osnovu
	 * udaljenosti od centra kruga.
	 */
	void proveri(Tacka t) {
		double d = this.centar.udaljenost(t);
		// double d = t.udaljenost(this.centar);
		if (d < this.r)
			System.out.println("unutar kruga.");
		else if (d == this.r)
			System.out.println("na krugu");
		else
			System.out.println("Izvan kruga");
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o instanceof Krug) {
			Krug k = (Krug)o;
			if (this.centar.equals(k.centar) && this.r == k.r) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}
