### SEMESTAR IV

# Operativni sistemi
[Predavanje 1](https://www.youtube.com/watch?v=GFjeU9YJpqE&feature=youtu.be&ab_channel=4.semestar)

| Obaveza | Datum | Vreme | Bodovi |
| ------------- | :-------------: | :-------------: | :-------------: |
| **Kolokvijum I** | 15.4.2021. | 15h do 20h | **30** |
| **Kolokvijum II** | 7.6.2021. | TBD | **30** |
| **Ispit** | 24.6.2021. | TBD | **40** |

**uslov:** zbir bodova sa projekata >= **30**

#### 1. Kolokvijum I
- Konkurentno programiranje
- 15:00 - 16:30 (grupa 1 i 2)
- 16:45 - 18:15 (grupa 3 i 4)
- 18:30 - 20:00 (grupa 5)
#### 2. Kolokvijum II
- Administracija operativnog sistema – komande za administraciju Linux operativnog sistema
#### 3. Ispit
- polaže se **usmeno**

# Specifikacija i modeliranje softvera

| Obaveza | Datum | Vreme | Bodovi |
| ------------- | :-------------: | :-------------: | :-------------: |
| **Kolokvijum 1** | 18.4.2021. | 18h do 20h | **30** |
| **Kolokvijum 2** | 29.5.2021. | od 18h | **30** |
| **Projekat** | 5.7.2021. | TBD | **40** |

Dodatni bonusi za aktivnost i kvalitet projekta.
#### Kolokvijum 1
- ovaj kolokvijum možete polagati ili popravljati još dva puta, u prva dva ispitna roka (junski i julski).
#### Projekat
- **timovi** od 3-4 člana
- moguće polaganje i u septembru, ali se bodovi množe sa 0.8
- **UPDATE** -> **Na jednom predavanju, profesorica je rekla da verovatno neće množiti bodove sa 0.8 ko ostavi projekat za kasnije, ali da se ne uzdamo u to**

# Sistemska programska podrška 1!jedan
- _**Knjiga**_: V. Kovačević i M. Popović, „Projektovanje asemblera, kompajlera i punjača", FTN Izdavaštvo, Novi Sad, 2015.
- [_**Materijali**_](https://www.rt-rk.uns.ac.rs/?q=predmeti/siit/spp1-sistemska-programska-podr%C5%A1ka-1)

| Obaveza | Bodovi |
| ------------- | :-------------: |
| Pohađanje predavanja i vežbi | **10** |
| Laboratorijske vežbe | **40** |
| Predmetni projekat | **20** |
| Ispit iz teorije | **30** |

**uslov:** ostvariti bar 25% bodova sa predispitnih obaveza

#### Ispit iz teorije
- Odrzace se **22.6.2021.**

# Paralelno programiranje
- _**Knjiga**_: М. Поповић и В. Ковачевић, „Паралелно програмирање“, ФТН Издаваштво, Нови Сад, 2015.
- [_**Materijali**_](https://www.rt-rk.uns.ac.rs/predmeti/siit/pp-paralelno-programiranje)

| Obaveza | Bodovi |
| ------------- | :-------------: |
| Laboratorijske vežbe | **30** |
| Predmetni projekat | **40** |
| Ispit iz teorije | **30** |

#### 1. Laboratorijske vežbe
- 13 termina
- prisustvo se evidentira, dobijaju se i nagrade (+) za aktivnost
- u svakom terminu se stiču bodovi
- u poslednja četiri termina se radi projekat
#### 2. Projekat
#### 3. Teorija
- pismeni ispit iz dva dela
- **DRUGI DIO 27.5.2021.**

# Baze podataka

| Obaveza | Datum | Vreme | Bodovi |
| ------------- | :-------------: | :-------------: | :-------------: |
| **Predmetni zadatak** | 6.6.2021. | TBD | **15** |
| **Praktična znanja** | 6.6.2021. | TBD | **55** |
| **Ispit** | 17-18.6.2021. | TBD | **30** |

**uslov:** ostvariti bar 51% od predviđenih 70 bodova sa predispitnih obaveza

### 1. Predispitne obaveze
#### Predmetni zadatak  (15b)
- pretežno teorijskog karaktera
#### 'Projekat' (55b)
##### - Znanja u vezi s ER modelom podataka [vežbe, 10b]
##### - Znanja u vezi s osnovima jezika SQL [vežbe + vežbe, 10b + 10b]
##### - Znanja u vezi s proceduralnim proširenjem jezika SQL – PL/SQL [vežbe + predmetni zadatak, 10b + 15b]

### 2. Ispit (30b)
- polaže se **usmeno**
