from math import sqrt

def kv_jednacina(a, b, c):
    D = sqrt(b**2-4*a*c)
    x1 = (-b+D)/2/a
    x2 = (-b-D)/2/a
    return x1, x2

def f():
    print("Ja sam funkcija f")

#print(__name__)
print(kv_jednacina(-1, 5, 5))