# OOP2 - SIIT - ISPIT 11.02.2018.

## ELIMINACIONI DEO

<br>

1. Ako je data klasa `A` koja implementira virtualnu metodu `foo()` i klasa `B` koja nasledjuje `A` i implementira istu tu metodu, kako treba kreirati promenljivu tipa `B` da bi mogli da pozovemo bas tu metodu `foo` iz klase `B` na nacin `x->foo()` ?

   ```c++
    a)  B* x = new A();
    b)  B y;
        B* x = &y;
    c)  B y;
        A* x = &y;
    d)  A* x = new B();
   ```

2. Zaokruziti ispravne pozive funkcija.

   ```c++
    int i;
    std::vector<int> v;
    void foo(int i, std::vector<int>* v);

    a) foo(int i, std::vector<int> v);
    b) foo(x,&y);
    c) //Nedostaje
    d) //Nedostaje
   ```

3. Dodavanjem novog elementa u listu L mozemo sa sigurnoscu tvrditi da:

   a) Kolicina memorije koju zauzima lista L ostaje linearno zavisna od broja elemenata te liste  
   b) Svi postojeci elementi ostaju na istim memorijskim lokacijama  
   c) Svi elementi unutar liste ce biti istog tipa  
   d) Svi postojeci elementi ostaju na istim pozicijama u listi

4. Za C++ mozemo tvrditi da je to:

   a) U potpunosti staticki tipski bezbedan jezik  
   b) U potpunosti dinamicki tipski bezbedan jezik  
   c) `std::string` je ugradjeni tip  
   d) Podrzava iskljucivo objektno orijentisanu paradigmu

5. Za dati isecak koda na odgovarajucem mestu dodati kod tako da se na standardnom izlazu ispise x

   ```c++
    namespace Djura
    {
        int x;
    }

    #include <iostream>
    void main()
    {
        // ovde dodati kod
    }
   ```

6. Napisati deklaraciju:

   a) PROMENLJIVE tipa `std::list` koja sadrzi elemente tipa pokazivac na vektor int brojeva  
   b) PROMENLJIVE korisnicki definisanog tipa MojTip  
   c) Slozenog tipa koji sadrzi dve javne promenljive tipa int i privatni konstruktor kopije

<br>

## GLAVNI DEO

<br>

1. Objasniti ideal staticke tipske bezbednosti

2. Sta su preduslovi i kada se proveravaju? Sta su postuslovi i kada se proveravaju?

3. Za dati isecak koda i svaki zadati prototip funkcije, napisati njen ispravan poziv

   ```c++
    int i;
    const int& j = i;
    std::map<string, void*> q;

    a) void foo(int a, std::map<string, void*> b, int& c)
    b) void foo(const int a, int *b, std::map<string, void*> c)
    c) void* foo(int a, int b, const std::map<string, void*> &c)
    d) ... i tako dalje, razne moguce kombinacije
    e) ...
    f) ...
   ```

4. Data su 2 header fajla i jedan cpp fajl. Dodati kod tako da se prvo vrsi poziv metode foo() iz prvog zaglavlja, a zatim iz drugog. Nije dozvoljeno menjanje koda. Dozvoljeno je dodati kod u header fajlove ako je to potrebno.

   ```c++
   <prvi.h>

       void foo() { /*...*/ }
   ```

   ```c++
   <drugi.h>

       void foo() { /*...*/ }
   ```

   ```c++
   <main.cpp>

       void main()
       {

       }
   ```

5. Na naznaceno mesto u datom isecku koda dodati kod koji ce omoguciti ocekivani ispis na standardni izlaz

   ```c++
   // ovde dodati kod

    void main(){

        std::vector<int> x = {1, 2};
        std::vector<int> y = {3, 4};
        std::vector<int> z = x + y;

        for(int i = 0; i < z.size(); ++i)
        {
            cout << z[i] << " ";
        }

        // ocekivani ispis je 1 2 3 4

    }
   ```

6. Napisati kod koji ce iz fajla "noviFajl" ucitavati stringove sa preskakanjem razmaka i ispisivati ih na standardni izlaz

7. Za dati kod napisati ocekivani izlaz

   ```c++
    struct a
    {
        void foo(int i) { cout << "0";};
    };

    struct b: a
    {
        void foo(int i) { cout << "1";};
    };

    struct c: b
    {
        void foo(int i) { cout << "2";};
    };

    // nakon ovoga je bilo dato jos 6-7 linija koda sa 4 deklarisana objekta, nekim pokazivacima i referencama, na najrazlicitije moguce nacine i kombinacije, i pozivale su se funkcije foo

    nesto u fazonu:

    b x;
    a* z = &x;
    a& w = x;
   ```

8. Za datu klasu napisati konstruktor kopije, i destruktor. Kopija koja se kreira mora biti duboka kopija. Obezbediti da ne dolazi do curenja memorije. Ispod napisati primer poziva konstruktora kopije.

   ```c++
    class MyType
    {
        int size;
        char* buff;

    public:
        // ovde dodati kod
    }

    // ovde napisati primer
   ```

9. Za dati sablon zaokruziti ispravne pozive funkcija, a neispravne precrtati.

   ```c++
    template<class T>
    T saberi(T x, T y)
    {
        return x + x + y + y;
    }

    int a;
    int b;
    float c;
    float d;

    a) saberi(a,b);
    b) template<int> saberi(a,b);
    c) <int> saberi(c,d);
    d) saberi(c,d) <int>;
    e) saberi(b,c);
    f) ...
   ```

10. Zapoceta je funkcija foo. Ona treba da prima mapu koja preslikava int na float, i da vraca invertovanu mapu, koja preslikava float na int. Na primer, ako mapa koja je argument preslikava 5 na 7.3 i 8 na 13.4, invertovana mapa treba da preslikava 7.3 na 5 i 13.4 n 8.  
    Dodatno objasniti kako biste istu stvar uradili sa std::string i char?
    ```c++
    void foo(
    ```
